import { Route } from '@angular/router';
import { CanLoadDashboardGuard } from './guards/can-load-dashboard.guard';
import { AuthComponent } from './pages/auth/auth.component';
import { RegisterComponent } from './pages/register/register.component';
import { InstallationComponent } from './pages/installation/installation.component';

export const APP_ROUTES: Route[] = [
    { path: 'login', component: AuthComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'install', component: InstallationComponent },
    {
        path: '',
        loadChildren: './pages/dashboard/dashboard.module#DashboardModule',
        canLoad: [CanLoadDashboardGuard],
    },
    { path: '**', redirectTo: '/404' },
];
