import { AuthActions, AuthTypes } from './auth.action';
import { IAccountInfo } from '../../models/interfaces/account-info';

export class AuthState {
    accountInfo: IAccountInfo;
}

export function reducer(state = new AuthState(), action: AuthActions): AuthState {
    switch (action.type) {
        case AuthTypes.SetAccountInfoSuccess:
            return {
                ...state,
                accountInfo: action.payload,
            };
        case AuthTypes.Logout:
            return {
                ...state,
                accountInfo: undefined,
            };
        default: {
            return state;
        }
    }
}
