import { Component, OnInit } from '@angular/core';
import { IAccountInfo } from '../../../../../models/interfaces/account-info';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../store';

@Component({
  selector: 'db-my-profile-dialog',
  templateUrl: './my-profile-dialog.component.html',
  styleUrls: ['./my-profile-dialog.component.scss'],
})
export class MyProfileDialogComponent implements OnInit {
  myProfile$: Observable<IAccountInfo>;

  constructor(
    private store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    this.myProfile$ = this.store.select(s => s.auth.accountInfo);
  }

}
