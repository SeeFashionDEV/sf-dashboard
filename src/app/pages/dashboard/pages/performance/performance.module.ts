import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../../../../core/core.module';
import { DashboardCoreModule } from '../../core/core.module';
import { MaterialModule } from '../../../../core/modules/material.module';
import { PerformanceComponent } from './performance.component';
import { DASHBOARD_SECTIONS } from './sections';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

@NgModule({
    imports: [
        CommonModule,
        CoreModule,
        DashboardCoreModule,
        MaterialModule,
        SatDatepickerModule,
        SatNativeDateModule,
    ],
    exports: [],
    declarations: [
        PerformanceComponent,
        ...DASHBOARD_SECTIONS,
    ],
    providers: [

    ],
})
export class PerformanceModule { }
