import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRecosComponent } from './recos.component';

describe('RecosComponent', () => {
    let component: DashboardRecosComponent;
    let fixture: ComponentFixture<DashboardRecosComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardRecosComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardRecosComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
