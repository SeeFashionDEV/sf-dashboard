import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardBar2Component } from './bar2.component';

describe('Bar2Component', () => {
    let component: DashboardBar2Component;
    let fixture: ComponentFixture<DashboardBar2Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardBar2Component],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardBar2Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
