import { MapRegion } from '../enums/map-region';
import { MapColor } from '../enums/map-colors';

export interface IMapRegion {
    region: MapRegion;
    label: string;
    color: MapColor;
    x: number;
    y: number;
    size: number;
    height: number;
}