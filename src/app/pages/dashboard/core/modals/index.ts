import { TopRecommendationsDialogComponent } from './top-recommendations-dialog/top-recommendations-dialog.component';
import { MyProfileDialogComponent } from './my-profile-dialog/my-profile-dialog.component';

export const DASHBOARD_MODALS = [
    TopRecommendationsDialogComponent,
    MyProfileDialogComponent,
];