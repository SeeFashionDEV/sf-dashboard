import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'db-blocks',
    templateUrl: './blocks.component.html',
    styleUrls: ['./blocks.component.scss'],
})
export class DashboardBlocksComponent implements OnInit {
    @Input() rounded: boolean;
    @Input() v1: string;
    @Input() v2: string;
    @Input() p1: number;
    @Input() p2: number;

    constructor() { }

    ngOnInit(): void {
    }

}
