import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardMapRegionComponent } from './map-region.component';

describe('MapRegionComponent', () => {
    let component: DashboardMapRegionComponent;
    let fixture: ComponentFixture<DashboardMapRegionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardMapRegionComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardMapRegionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
