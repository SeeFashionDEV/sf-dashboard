import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../store';
import { Subscription } from 'rxjs';
import { IRecommendation } from '../../../../../../models/interfaces/recomendation';
import { topRecommendationsState } from '../../../../store';
import {
    OpenTopRecommendationDialogAction, LoadMoreTopRecommendationsAction, LoadAllTopRecommendationsAction,
} from '../../../../store/recommendations/recommendations.action';
import { fadeIn } from '../../../../../../core/animations/fade.animation';

@Component({
    selector: 'db-top-recomendations',
    templateUrl: './top-recomendations.component.html',
    styleUrls: ['./top-recomendations.component.scss'],
    animations: [fadeIn],
})
export class DashboardTopRecomendationsComponent implements OnInit, OnDestroy {
    cardsSub$: Subscription;
    recommendations: IRecommendation[] = [];
    recommendationSize: number = 0;
    isLoading: boolean;
    isTableDisplayed: boolean;
    @ViewChild('container') container: ElementRef;

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.cardsSub$ = this.store.select(topRecommendationsState).subscribe(obj => {
            if (!obj) return;
            this.recommendations = obj.topRecommendations;
            this.isLoading = obj.isCardsLoading;
            this.recommendationSize = obj.recommendationsLength;
            this.isTableDisplayed = obj.isTableDispalayed;
        });
    }

    ngOnDestroy(): void {
        if (this.cardsSub$) this.cardsSub$.unsubscribe();
    }

    openRecommendationDialog(card: IRecommendation): void {
        this.store.dispatch(new OpenTopRecommendationDialogAction(card));
    }

    loadMore(length: number): void {
        this.store.dispatch(new LoadMoreTopRecommendationsAction(length + 8));
    }

    loadAll(): void {
        this.store.dispatch(new LoadAllTopRecommendationsAction(this.recommendationSize));
        // this.container.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }

}
