import { IGeneralInfoData } from './general-info-data';

export interface IGeneralInfo {
    currency: string;
    totalRevenue: IGeneralInfoData;
    averageOrder: IGeneralInfoData;
    conversionRate: IGeneralInfoData;
    productViews: IGeneralInfoData;
    revenuesChartData: any;
}
