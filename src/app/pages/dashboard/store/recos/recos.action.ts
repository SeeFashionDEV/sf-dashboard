import { Action } from '@ngrx/store';
import { IVisitorsAndSales } from '../../../../models/interfaces/visitors-and-sales';
import { IRecos } from '../../../../models/interfaces/recos';

export enum RecosTypes {
    LoadData = '[RECOS] Load data [...]',
    LoadDataSuccess = '[RECOS] Load data [SUCCESS]',
    LoadDataError = '[RECOS] Load data [ERROR]',
}

// LOAD DATA
export class LoadRecosDataAction implements Action {
    readonly type = RecosTypes.LoadData;
}

export class LoadRecosDataSuccessAction implements Action {
    readonly type = RecosTypes.LoadDataSuccess;

    constructor(public payload: IRecos) { }
}

export class LoadRecosDataErrorAction implements Action {
    readonly type = RecosTypes.LoadDataError;

    constructor(public payload: any) { }
}

export type RecosActions
    = LoadRecosDataAction
    | LoadRecosDataSuccessAction
    | LoadRecosDataErrorAction
    ;