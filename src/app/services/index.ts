import { LocalStorageService } from './local-storage.service';
import { ConfigService } from './config.service';
import { AuthService } from './auth.service';
import { HttpService } from './http.service';
import { ScrollService } from './scroll.service';
import { RegisterService } from './register.service';
import { InstallationService } from './installation.service';

export const APP_SERVICES = [
    LocalStorageService,
    ConfigService,
    AuthService,
    RegisterService,
    InstallationService,
    HttpService,
    ScrollService,
];
