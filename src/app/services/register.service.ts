import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IAccountInfo } from '../models/interfaces/account-info';
import { IRegister } from '../models/interfaces/IRegister';

@Injectable()
export class RegisterService {

    constructor(
        private config: ConfigService,
        private http: HttpClient,
    ) { }

    get apiUrl(): string {
        return this.config.apiUrl;
    }

    register(creds: IRegister): Observable<HttpResponse<IAccountInfo>> {
        return this.http.post<IAccountInfo>(`${this.apiUrl}/register`, creds, { observe: 'response' });
    }

}
