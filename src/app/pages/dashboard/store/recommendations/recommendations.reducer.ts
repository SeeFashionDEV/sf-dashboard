import { RecommendationsActions, RecommendationsTypes } from './recommendations.action';
import { IRecommendation } from '../../../../models/interfaces/recomendation';

export class TopRecommendationsState {
    topRecommendations: IRecommendation[];
    isCardsLoading: boolean;
    recommendationsLength: number;
    isTableDispalayed: boolean;
}

export function reducer(
    state = new TopRecommendationsState(),
    action: RecommendationsActions): TopRecommendationsState {
    switch (action.type) {
        case RecommendationsTypes.LoadTopRecommendations:
            return {
                ...state,
                isCardsLoading: true,
            };
        case RecommendationsTypes.LoadTopRecommendationsSuccess:
            return {
                ...state,
                isCardsLoading: false,
                topRecommendations: action.payload,
                isTableDispalayed: false,
            };
        case RecommendationsTypes.LoadTopRecommendationsError:
            return {
                ...state,
                isCardsLoading: false,
            };
        case RecommendationsTypes.LoadMoreTopRecommendations:
            return {
                ...state,
                isCardsLoading: true,
            };
        case RecommendationsTypes.SetTopRecommendationsLength:
            return {
                ...state,
                recommendationsLength: action.payload,
            };
        case RecommendationsTypes.LoadAllTopRecommendations:
            return {
                ...state,
                isCardsLoading: true,
            };
        case RecommendationsTypes.LoadAllTopRecommendationsSuccess:
            return {
                ...state,
                isCardsLoading: false,
                isTableDispalayed: true,
                topRecommendations: action.payload,
            };
        case RecommendationsTypes.LoadAllTopRecommendationsError:
            return {
                ...state,
                isCardsLoading: false,
            };
        default: {
            return state;
        }
    }
}
