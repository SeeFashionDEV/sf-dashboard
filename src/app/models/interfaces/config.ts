export interface IAppConfig {
    apiUrl: string;
    serverUrl: string;
}
