import { Component } from '@angular/core';
import { IFilterState } from '../../../../../../models/interfaces/filter-state';
import { Store } from '@ngrx/store';
import { ChangeFilterStateAction } from '../../../../store/filter/filter.action';
import { FilterRange } from '../../../../../../models/enums/filter-range';
import { AppState } from '../../../../../../store/index';

@Component({
    selector: 'db-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss'],
})
export class DashboardFilterComponent {
    FilterRange = FilterRange;
    filterState: IFilterState = {
        range: FilterRange.MONTH,
    };
    helpMessage = 'Help';

    constructor(
        private store: Store<AppState>,
    ) {
    }

    change(range: FilterRange): void {
        this.filterState = {
            range: range,
        };
        this.store.dispatch(new ChangeFilterStateAction(this.filterState));
    }

    onCustomRange(value): void {
        console.log(value);
        this.filterState = {
            custom: {
                startDate: value.begin,
                endDate: value.end,
            },
        };
        this.store.dispatch(new ChangeFilterStateAction(this.filterState));
    }

}
