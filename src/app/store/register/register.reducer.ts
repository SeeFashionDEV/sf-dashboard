import { RegActions, RegisterTypes } from './register.action';
import { IAccountInfo } from '../../models/interfaces/account-info';

export class RegState {
    accountInfo: IAccountInfo;
}

export function reducer(state = new RegState(), action: RegActions): RegState {
    switch (action.type) {
        case RegisterTypes.SetAccountInfoSuccess:
            return {
                ...state,
                accountInfo: action.payload,
            };
        default: {
            return state;
        }
    }
}
