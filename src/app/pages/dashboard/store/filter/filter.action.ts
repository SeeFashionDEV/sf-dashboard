import { Action } from '@ngrx/store';
import { IFilterState } from '../../../../models/interfaces/filter-state';

export enum FilterTypes {
    ChangeFilterState = '[FILTER] Change',
}

// Change filter option
export class ChangeFilterStateAction implements Action {
    readonly type = FilterTypes.ChangeFilterState;

    constructor(public payload: IFilterState) { }
}

export type FilterActions
    = ChangeFilterStateAction
    ;