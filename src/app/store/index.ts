import { ActionReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { MetaReducer } from '@ngrx/store';
import { ActionReducerMap } from '@ngrx/store';
import * as auth from './auth/auth.reducer';
import * as reg from './register/register.reducer';
import { environment } from '../../environments/environment';
import { AuthEffects } from './auth/auth.effect';
import { RegisterEffects } from './register/register.effect';
import { InstallationEffect } from './installation/installation.effect';


export interface AppState {
    auth: auth.AuthState;
    reg: reg.RegState;
}

export const appReducers: ActionReducerMap<AppState> = {
    auth: auth.reducer,
    reg: reg.reducer,
};

export const appEffects = [
    AuthEffects,
    RegisterEffects,
    InstallationEffect,

];

export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
    return function (state: AppState, action: any): AppState {
        console.log(state, action);
        return reducer(state, action);
    };
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production
    ? [logger, storeFreeze]
    : [];
