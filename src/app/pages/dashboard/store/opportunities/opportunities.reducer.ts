import { IRecommendation } from '../../../../models/interfaces/recomendation';
import { OpportunitiesTypes, OpportunitiesActions } from './opportunities.action';
import { MapRegion } from '../../../../models/enums/map-region';
import { IOpportunities, IOpportunitiesTotalRevenue } from '../../../../models/interfaces/opportunities';

export class OpportunitiesState implements IOpportunities {
    totalRevenue: IOpportunitiesTotalRevenue;
    mostPopular: IRecommendation[];
    missedOpportunities: IRecommendation[];
    activeRegion: MapRegion = MapRegion.UnitedKingdom;
}

export function reducer(state = new OpportunitiesState(), action: OpportunitiesActions): OpportunitiesState {
    switch (action.type) {
        case OpportunitiesTypes.LoadDataSuccess:
            return {
                ...state,
                totalRevenue: action.payload.totalRevenue,
                mostPopular: action.payload.mostPopular,
                missedOpportunities: action.payload.missedOpportunities,
            };
        case OpportunitiesTypes.SelectRegion:
            return {
                ...state,
                activeRegion: action.payload,
            };
        default: {
            return state;
        }
    }
}
