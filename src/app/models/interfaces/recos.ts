import { IInfoBlockData } from './info-block-data';

export interface IRecosBlock {
    renderedRecoBoxes: IInfoBlockData;
    ctr: IInfoBlockData;
    conversions: IInfoBlockData;
    totalSales: IInfoBlockData;
}

export interface IRecos {
    stylePicker: IRecosBlock;
    internalPages: IRecosBlock;
    outOfStockPopup: IRecosBlock;
}