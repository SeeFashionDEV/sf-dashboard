import { Component, OnInit, ElementRef, ViewChild, Renderer2, NgZone, AfterViewInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { SetAccountInfoAction } from '../../store/auth/auth.action';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
    isHandset$: Observable<boolean>;
    isVisible: boolean;
    listenerFn: Function;
    @ViewChild('pageWrapper') pageWrapperRef: ElementRef;

    constructor(
        private renderer: Renderer2,
        private zone: NgZone,
        private breakpointObserver: BreakpointObserver,
        private store: Store<AppState>,
    ) {
    }

    ngOnInit(): void {
        this.store.dispatch(new SetAccountInfoAction());
        this.isHandset$ = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
            map(result => result.matches),
        );
    }

    ngAfterViewInit(): void {
        this.zone.runOutsideAngular(() => {
            this.listenerFn = this.renderer.listen(this.pageWrapperRef.nativeElement, 'scroll', (event) => {
                this.onScroll(event);
            });
        });
    }

    ngOnDestroy(): void {
        if (this.listenerFn) this.listenerFn();
    }

    onScroll(e): void {
        if (this.isVisible == e.target.scrollTop > 300) return;
        this.zone.run(() => {
            this.isVisible = e.target.scrollTop > 300;
        });
    }
}
