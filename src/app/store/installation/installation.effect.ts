import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { tap, map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { Observable, empty, of } from 'rxjs';
import { LocalStorageService } from '../../services/local-storage.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import {
    InstallActions,
    InstallationAction, InstallationErrorAction,
    InstallationSuccessAction,
    InstallationTypes,
} from './installation.action';
import { InstallationService } from '../../services/installation.service';

@Injectable()
export class InstallationEffect {

    @Effect({dispatch: false})
    onRegister$: Observable<InstallActions> = this.actions$.pipe(
        ofType<InstallationAction>(InstallationTypes.Install),
        tap(data => {
            // this.installService.install();
            this.router.navigate(['/register']);
        }),
    );

    constructor(
        private actions$: Actions,
        private installService: InstallationService,
        private localStorage: LocalStorageService,
        private router: Router,
        private dialog: MatDialog,
    ) {
    }
}
