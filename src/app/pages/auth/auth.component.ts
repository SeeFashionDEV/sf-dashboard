import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/index';
import { LoginAction } from '../../store/auth/auth.action';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
    form: FormGroup;

    constructor(
        private store: Store<AppState>,
        private fb: FormBuilder,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
        });
    }

    onLogin(): void {
        if (this.form.invalid) return;
        const creds = this.form.value;
        this.store.dispatch(new LoginAction(creds));
    }

    goToRegister(): void {
        this.router.navigate([`/install`]);
    }

    goToForgotPassword(): void {
        this.router.navigate([`/recover`]);
    }
}
