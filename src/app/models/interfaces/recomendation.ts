export interface IRecommendation {
    createdAt: string;
    id: number;
    itemsOnStock: number;
    noOfClick: number;
    productId: number;
    productImage: string;
    productTitle: string;
    shopId: number;
    soldItems: number;
}
