#!/usr/bin/env bash

while [ true ]
do
    if [ "$(curl -I -s -X GET https://dashboard.seefashion-insights.com | head -n 1 | cut -d$' ' -f2)" = '200' ]
    then
        exit 0
    else
        echo "check UI server is running?"
        sleep 3s
    fi
done
