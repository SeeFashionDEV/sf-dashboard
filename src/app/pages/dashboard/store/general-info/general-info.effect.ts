// import { Injectable } from '@angular/core';
// import { Effect, Actions, ofType } from '@ngrx/effects';
// import { Observable, of, empty } from 'rxjs';
// import {
//     GeneralInfoActions, LoadGeneralInfoDataAction, GeneralInfoTypes,
// } from './general-info.action';
// import { AppState } from '../../../../store/index';
// import { Store } from '@ngrx/store';
// import { withLatestFrom, switchMap } from 'rxjs/operators';
// import { getFilterState } from '../index';
// import { FilterRange } from '../../../../models/enums/filter-range';
// import { IGeneralInfoData } from '../../../../models/interfaces/general-info-data';
// import { DataType } from '../../../../models/enums/data-type';
// import { FilterActions, FilterTypes } from '../filter/filter.action';
//
// @Injectable()
// export class GeneralInfoEffects {
//
//     @Effect()
//     loadData$: Observable<GeneralInfoActions> = this.actions$.pipe(
//         ofType<LoadGeneralInfoDataAction>(GeneralInfoTypes.LoadData),
//         switchMap(([action, filter]) => {
//             if (!filter) return empty();
//             const totalRevenue = this.getTotalRevenueData(filter.range);
//             const averageOrder = this.getAverageOrderData(filter.range);
//             const conversionRate = this.getConversionsRateData(filter.range);
//             const productViews = this.getProductViewsData(filter.range);
//             return of(new LoadGeneralInfoDataSuccessAction({
//                 totalRevenue: totalRevenue,
//                 averageOrder: averageOrder,
//                 conversionRate: conversionRate,
//                 productViews: productViews,
//             }));
//         }),
//     );
//
//     @Effect()
//     reloadData$: Observable<GeneralInfoActions> = this.actions$.pipe(
//         ofType<FilterActions>(FilterTypes.ChangeFilterState),
//         switchMap(() => of(new LoadGeneralInfoDataAction())),
//     );
//
//     constructor(
//         private actions$: Actions,
//         private store: Store<AppState>,
//     ) { }
//
//
//     getTotalRevenueData(range: FilterRange): IGeneralInfoData {
//         if (range == null) return;
//         const v = {
//             title: 'total revenue',
//             // tslint:disable:max-line-length
//             helpMessage: `The total revenue of all products sold on your website, including: \n
//             - the proportion of total revenue generated through SeeFashion (recommendations) versus standard navigation on your website\n
//             - The percentage change in the total revenue over the selected time period\n
//             - the percentage change in the revenues generated through See Fashion versus standard navigation on your website over the selected time period.`,
//             dataType: DataType.CURRENCY,
//         };
//         let total;
//         let standard;
//         let seeFashion;
//         switch (range) {
//
//             case FilterRange.MONTH:
//                 total = {
//                     value: 145019.5,
//                     kpi: 5,
//                 };
//                 standard = {
//                     value: 101513.6,
//                     kpi: 3,
//                 };
//                 seeFashion = {
//                     value: 43505.8,
//                     kpi: 8,
//                 };
//                 break;
//             case FilterRange.WEEK:
//                 total = {
//                     value: 31187,
//                     kpi: 14,
//                 };
//                 standard = {
//                     value: 19959.6,
//                     kpi: 10,
//                 };
//                 seeFashion = {
//                     value: 11227.3,
//                     kpi: 18,
//                 };
//                 break;
//             case FilterRange.TODAY:
//                 total = {
//                     value: 4798,
//                     kpi: 5.5,
//                 };
//                 standard = {
//                     value: 3742.4,
//                     kpi: 5,
//                 };
//                 seeFashion = {
//                     value: 1055.5,
//                     kpi: 6,
//                 };
//                 break;
//         }
//         return {
//             ...v,
//             total: total,
//             standard: standard,
//             seeFashion: seeFashion,
//         };
//     }
//
//     getAverageOrderData(range: FilterRange): IGeneralInfoData {
//         if (range == null) return;
//         const v = {
//             title: 'average order value',
//             helpMessage: `Shows the average order value of a single purchase on your website, including:\n
//             - The average contribution made by See Fashion recommendations to a single purchase versus the average order value without SeeFashion recommendations\n
//             - The percentage change in the overall average order value over the selected time period\n
//             - The percentage change in the average contribution made by See Fashion recommendations to a single purchase versus the average order value without SeeFashion recommendations over the selected time period`,
//             dataType: DataType.CURRENCY,
//         };
//         let standard;
//         let seeFashion;
//         let total;
//         switch (range) {
//
//             case FilterRange.MONTH:
//                 total = {
//                     value: 139,
//                     kpi: 3,
//                 };
//                 standard = {
//                     value: 121,
//                     kpi: -3,
//                 };
//                 seeFashion = {
//                     value: 157,
//                     kpi: 9,
//                 };
//                 break;
//             case FilterRange.WEEK:
//                 total = {
//                     value: 144,
//                     kpi: 6,
//                 };
//                 standard = {
//                     value: 120,
//                     kpi: -10,
//                 };
//                 seeFashion = {
//                     value: 168,
//                     kpi: 22,
//                 };
//                 break;
//             case FilterRange.TODAY:
//                 total = {
//                     value: 152.5,
//                     kpi: 1.1,
//                 };
//                 standard = {
//                     value: 140,
//                     kpi: -5,
//                 };
//                 seeFashion = {
//                     value: 165,
//                     kpi: 7.3,
//                 };
//                 break;
//         }
//         return {
//             ...v,
//             total: total,
//             standard: standard,
//             seeFashion: seeFashion,
//         };
//     }
//
//     getConversionsRateData(range: FilterRange): IGeneralInfoData {
//         if (range == null) return;
//         const v = {
//             title: 'coversion rate',
//             helpMessage: `The percentage of unique visitors that place an order on your website, including:\n
//             - The percentage of unique visitors that placed an order with a SeeFashion recommended product (SeeFashion conversion rate) versus percentage of unique visitors that placed an order without a SeeFashion recommended product (Standard navigation conversion rate).\n
//             - The percentage change in the overall conversion rate over the selected time period\n
//             - The percentage change in the conversion rate with SeeFashion recommendations versus standard navigation on your website over the selected time period.`,
//             dataType: DataType.PERCENTAGE,
//         };
//         let standard;
//         let seeFashion;
//         let total;
//         switch (range) {
//             case FilterRange.MONTH:
//                 total = {
//                     value: 5.1,
//                     kpi: 2,
//                 };
//                 standard = {
//                     value: 4,
//                     kpi: -1,
//                 };
//                 seeFashion = {
//                     value: 6.2,
//                     kpi: 5,
//                 };
//                 break;
//             case FilterRange.WEEK:
//                 total = {
//                     value: 5.4,
//                     kpi: 4,
//                 };
//                 standard = {
//                     value: 4.8,
//                     kpi: -5,
//                 };
//                 seeFashion = {
//                     value: 6,
//                     kpi: 13,
//                 };
//                 break;
//             case FilterRange.TODAY:
//                 total = {
//                     value: 4.1,
//                     kpi: 0.9,
//                 };
//                 standard = {
//                     value: 2.2,
//                     kpi: -2.5,
//                 };
//                 seeFashion = {
//                     value: 6,
//                     kpi: 4.3,
//                 };
//                 break;
//         }
//         return {
//             ...v,
//             total: total,
//             standard: standard,
//             seeFashion: seeFashion,
//         };
//     }
//
//     getProductViewsData(range: FilterRange): IGeneralInfoData {
//         if (range == null) return;
//         const v = {
//             title: 'product views',
//             helpMessage: `Total views for all products on your website, including:
//             - The proportion of total products viewed via a SeeFashion recommendation versus standard navigation\n
//             - The percentage change in total product views over a selected time period\n
//             - The percentage change in total products viewed via a SeeFashion recommendation versus standard navigation over the selected time period`,
//             dataType: DataType.NUMERIC,
//         };
//         let standard;
//         let seeFashion;
//         let total;
//         switch (range) {
//             case FilterRange.MONTH:
//                 total = {
//                     value: 47887.7,
//                     kpi: 8,
//                 };
//                 standard = {
//                     value: 23490.7,
//                     kpi: 1,
//                 };
//                 seeFashion = {
//                     value: 18898.7,
//                     kpi: 15,
//                 };
//                 break;
//             case FilterRange.WEEK:
//                 total = {
//                     value: 11695.1,
//                     kpi: 20,
//                 };
//                 standard = {
//                     value: 6387.1,
//                     kpi: 5,
//                 };
//                 seeFashion = {
//                     value: 4811.7,
//                     kpi: 35,
//                 };
//                 break;
//             case FilterRange.TODAY:
//                 total = {
//                     value: 1160.9,
//                     kpi: 10,
//                 };
//                 standard = {
//                     value: 823.3,
//                     kpi: 2.5,
//                 };
//                 seeFashion = {
//                     value: 422.2,
//                     kpi: 17.5,
//                 };
//                 break;
//         }
//         return {
//             ...v,
//             total: total,
//             standard: standard,
//             seeFashion: seeFashion,
//         };
//     }
// }
