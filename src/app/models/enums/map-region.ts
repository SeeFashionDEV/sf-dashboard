export enum MapRegion {
    WestCoast,
    EastCoast,
    SouthAmerica,
    UnitedKingdom,
    NorthAfrica,
    SouthAfrica,
    MoscowArea,
    IndiaAndOceania,
    Japan,
    Australia,
}