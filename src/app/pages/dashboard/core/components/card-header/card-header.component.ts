import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'db-card-header',
    templateUrl: './card-header.component.html',
    styleUrls: ['./card-header.component.scss'],
})
export class DashboardCardHeaderComponent implements OnInit {
    @Input() title: string;
    @Input() hint: string;
    @Input() helpMessage: string;

    constructor() { }

    ngOnInit(): void {
    }

}
