import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class LocalStorageService {
    helper = new JwtHelperService();

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
    ) { }

    supports_html5_storage(): boolean {
        if (isPlatformServer(this.platformId)) return;

        try {
            return window && 'localStorage' in window && window['localStorage'] !== undefined;
        } catch (e) {
            console.warn('Current browser doesn\'t support local storage');
            return false;
        }
    }

    setItem(key: string, value: any): boolean {
        if (isPlatformServer(this.platformId) || !this.supports_html5_storage()) return;
        localStorage.setItem(key, value);
        return true;
    }

    getItem(key: string): string | undefined {
        if (isPlatformServer(this.platformId) || !this.supports_html5_storage()) return;

        return localStorage.getItem(key);
    }

    removeItem(key: string): boolean {
        if (isPlatformServer(this.platformId) || !this.supports_html5_storage()) return;

        localStorage.removeItem(key);
        return true;
    }

    isTokenExpired(key: string): boolean {
        const token = this.getItem(key);
        if (!token) return false;
        try {
            return this.helper.isTokenExpired(token);
        } catch (e) {
            return false;
        }
    }
}