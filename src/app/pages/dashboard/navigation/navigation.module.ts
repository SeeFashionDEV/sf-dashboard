import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../../../core/core.module';
import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import { DropdownMenuComponent } from './header/dropdown-menu/dropdown-menu.component';
import { DropdownMenuItemComponent } from './header/dropdown-menu/dropdown-menu-item/dropdown-menu-item.component';
import { RouterModule } from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
		CoreModule,
		RouterModule,
    ],
    exports: [
        MenuComponent,
        HeaderComponent,
    ],
    declarations: [
        MenuComponent,
		HeaderComponent,
		DropdownMenuComponent,
		DropdownMenuItemComponent,

    ],
    providers: [

    ],
})
export class NavigationModule { }
