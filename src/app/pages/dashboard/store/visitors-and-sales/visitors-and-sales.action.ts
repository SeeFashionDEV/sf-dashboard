import { Action } from '@ngrx/store';
import { IVisitorsAndSales } from '../../../../models/interfaces/visitors-and-sales';

export enum VisitorsAndSalesTypes {
    LoadData = '[VISITORS AND SALES] Load data [...]',
}

// LOAD DATA
export class LoadVisitorsAndSalesDataAction implements Action {
    readonly type = VisitorsAndSalesTypes.LoadData;

    constructor(public payload: IVisitorsAndSales) { }
}

export type VisitorsAndSalesActions
    = LoadVisitorsAndSalesDataAction
    ;
