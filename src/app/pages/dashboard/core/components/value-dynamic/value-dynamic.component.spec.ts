import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueDynamicComponent } from './value-dynamic.component';

describe('ValueDynamicComponent', () => {
    let component: ValueDynamicComponent;
    let fixture: ComponentFixture<ValueDynamicComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ValueDynamicComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ValueDynamicComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
