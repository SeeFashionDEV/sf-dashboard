import { Component, OnInit, Input } from '@angular/core';
import { AppState } from '../../../../store/index';
import { Store } from '@ngrx/store';
import { LogoutAction, OpenProfileDialogsAction } from '../../../../store/auth/auth.action';
import { MatDrawer } from '@angular/material';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
    @Input() drawer: MatDrawer;
    @Input() isHandset: boolean;

    constructor(
        private store: Store<AppState>,
        private breakpointObserver: BreakpointObserver,
    ) { }

    ngOnInit(): void {

    }

    logout(): void {
        this.store.dispatch(new LogoutAction());
    }

    openProfile(): void {
        this.store.dispatch(new OpenProfileDialogsAction());
    }
}
