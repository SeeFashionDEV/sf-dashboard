import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCardHeaderComponent } from './card-header.component';

describe('CardHeaderComponent', () => {
  let component: DashboardCardHeaderComponent;
  let fixture: ComponentFixture<DashboardCardHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardCardHeaderComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardCardHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
