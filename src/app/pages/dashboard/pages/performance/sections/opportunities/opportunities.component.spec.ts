import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardOpportunitiesComponent } from './opportunities.component';

describe('OpportunitiesComponent', () => {
    let component: DashboardOpportunitiesComponent;
    let fixture: ComponentFixture<DashboardOpportunitiesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardOpportunitiesComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardOpportunitiesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
