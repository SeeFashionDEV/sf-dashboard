import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardVisitorsAndSalesComponent } from './visitors-and-sales.component';

describe('VisitorsAndSalesComponent', () => {
    let component: DashboardVisitorsAndSalesComponent;
    let fixture: ComponentFixture<DashboardVisitorsAndSalesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardVisitorsAndSalesComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardVisitorsAndSalesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
