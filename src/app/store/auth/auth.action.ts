import { Action } from '@ngrx/store';
import { ICredentials } from '../../models/interfaces/credentials';
import { IAccountInfo } from '../../models/interfaces/account-info';

export enum AuthTypes {
    Login = '[AUTH] Log in [...]',
    LoginSuccess = '[AUTH] Log in [SUCCESS]',
    LoginError = '[AUTH] Log in [ERROR]',
    SetAccountInfo = '[AUTH] Set account info',
    SetAccountInfoSuccess = '[AUTH] Set account info [SUCCESS]',
    SetAccountInfoError = '[AUTH] Set account info [ERROR]',
    Logout = '[AUTH] Log out',
    OpenProfileDialog = '[AUTH] Open Profile Dialog [...]',
}

// LOG IN
export class LoginAction implements Action {
    readonly type = AuthTypes.Login;

    constructor(public payload: ICredentials) { }
}

export class LoginSuccessAction implements Action {
    readonly type = AuthTypes.LoginSuccess;

    constructor(public payload: { token: string, accountInfo: any }) { }
}

export class LoginErrorAction implements Action {
    readonly type = AuthTypes.LoginError;

    constructor(public payload: any) { }
}

// SET USER DATA
export class SetAccountInfoAction implements Action {
    readonly type = AuthTypes.SetAccountInfo;
}

export class SetAccountInfoSuccessAction implements Action {
    readonly type = AuthTypes.SetAccountInfoSuccess;

    constructor(public payload: IAccountInfo) { }
}

export class SetAccountInfoErrorAction implements Action {
    readonly type = AuthTypes.SetAccountInfoError;

    constructor(public payload: any) { }
}

// LOG OUT
export class LogoutAction implements Action {
    readonly type = AuthTypes.Logout;
}

export class OpenProfileDialogsAction implements Action {
    readonly type = AuthTypes.OpenProfileDialog;
}

export type AuthActions
    = LoginAction
    | LoginSuccessAction
    | LoginErrorAction
    | SetAccountInfoAction
    | SetAccountInfoSuccessAction
    | SetAccountInfoErrorAction
    | LogoutAction
    | OpenProfileDialogsAction
    ;