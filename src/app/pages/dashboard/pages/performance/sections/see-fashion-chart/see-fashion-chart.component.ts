import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'db-see-fashion-chart',
  templateUrl: './see-fashion-chart.component.html',
  styleUrls: ['./see-fashion-chart.component.scss'],
})
export class DashboardSeeFashionChartComponent implements OnInit {
  chartData: any[] = [
    { label: 'Visitors', data: [12, 19, 3, 5, 2, 3, 12, 12, 19, 3], tot: 236 },
    { label: 'Add to cart', data: [7, 8, 3, 12, 2, 3, 4, 12, 5, 3], tot: 136 },
    { label: 'Sales', data: [8, 2, 35, 66, 2, 3, 65, 12, 89, 33], tot: 50 },
  ];

  totCounter: number[] = [
    236, 136, 50,
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
