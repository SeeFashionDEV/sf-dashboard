import { RadioButtonComponent } from "./inputs/radio-button/radio-button.component";

export const CORE_COMPONENTS = [
	RadioButtonComponent,
];