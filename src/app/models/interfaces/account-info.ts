import { IAuthority } from './authority';

export interface IAccountInfo {
    accountNonExpired: boolean;
    accountNonLocked: boolean;
    active: number;
    authorities: IAuthority[];
    credentialsNonExpired: boolean;
    email: string;
    enabled: boolean;
    firstName: string;
    lastName: string;
    password?: string;
    role: string;
    shopifyId: number;
    username: string;
}