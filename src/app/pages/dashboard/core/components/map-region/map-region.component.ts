import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMapRegion } from '../../../../../models/interfaces/map-region';
import { MapRegion } from '../../../../../models/enums/map-region';
import { AppState } from '../../../../../store/index';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { getActiveRegion } from '../../../store';

@Component({
    selector: 'db-map-region',
    templateUrl: './map-region.component.html',
    styleUrls: ['./map-region.component.scss'],
})
export class DashboardMapRegionComponent implements OnInit {
    activeRegion$: Observable<MapRegion>;

    @Input() region: IMapRegion;
    @Output() select: EventEmitter<MapRegion> = new EventEmitter();

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.activeRegion$ = this.store.select(getActiveRegion);
    }

    get circleStyle(): Object {
        const style = {};
        if (!this.region) return;
        style['width'] = `${this.region.size}%`;
        // style['background-color'] = `#${this.region.color}`;
        style['padding-top'] = `${this.region.size}%`;
        return style;
    }

    get mainStyle(): Object {
        const style = {};
        if (!this.region) return;
        style['top'] = `${this.region.y}%`;
        style['left'] = `${this.region.x}%`;
        return style;
    }

    get labelContainerStyle(): Object {
        const style = {};
        if (!this.region) return;
        style['left'] = `${this.region.size / 2}%`;
        style['height'] = `${this.region.height}%`;
        // style['border-color'] = `#${this.region.color}`;
        return style;
    }

    get labelStyle(): Object {
        const style = {};
        if (!this.region) return;
        // style['color'] = `#${this.region.color}`;
        return style;
    }

    onClick(e: MouseEvent): void {
        this.select.emit(this.region.region);
    }
}
