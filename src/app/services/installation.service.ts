import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IInstall } from '../models/interfaces/IInstall';
import { Status } from '../models/interfaces/status';

@Injectable()
export class InstallationService {

    constructor(
        private config: ConfigService,
        private http: HttpClient,
    ) { }

    get apiUrl(): string {
        return this.config.apiUrl;
    }

    get serverUrl(): string {
        return this.config.serverUrl;
    }

    install(data: IInstall): void {
        const url = `${this.serverUrl}/api/shopify/index?shop=${data.shopDomain}&timestamp=10000000`;
        console.log('aaaaa', url);
    }

}
