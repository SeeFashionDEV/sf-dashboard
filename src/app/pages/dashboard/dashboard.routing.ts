import { Route } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { PerformanceComponent } from './pages/performance/performance.component';

export const DASHBOARD_ROUTES: Route[] = [
    {
        path: '', component: DashboardComponent, children: [
            { path: '', redirectTo: 'performance' },
            { path: 'performance', component: PerformanceComponent },
        ],
    },
];
