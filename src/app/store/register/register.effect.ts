import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { tap, map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { Observable, empty, of } from 'rxjs';
import {
    RegisterTypes,
    RegisterErrorAction,
    RegisterAction,
    RegActions,
    RegisterSuccessAction,
    SetAccountInfoAction, SetAccountInfoSuccessAction, SetAccountInfoErrorAction,
} from './register.action';
import { LocalStorageService } from '../../services/local-storage.service';
import { StorageItem } from '../../models/enums/storage-item';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import {
    MyProfileDialogComponent,
} from '../../pages/dashboard/core/modals/my-profile-dialog/my-profile-dialog.component';
import { RegisterService } from '../../services/register.service';
import { IAccountInfo } from '../../models/interfaces/account-info';

@Injectable()
export class RegisterEffects {
    // @Effect({dispatch: false})
    // onOpenRegister$: Observable<Action> = this.actions$.pipe(
    //     ofType<OpenRegisterAction>(RegisterTypes.OpenRegister),
    //     tap(() => {
    //         this.localStorage.removeItem(StorageItem.Token);
    //         this.localStorage.removeItem(StorageItem.AccountInfo);
    //         this.router.navigate(['/register']);
    //     }),
    // );

    @Effect({dispatch: false})
    onRegister$: Observable<RegActions> = this.actions$.pipe(
        ofType<RegisterAction>(RegisterTypes.Register),
        map(action => action.payload),
        switchMap(creds =>
            this.registerService.register(creds).pipe(
                map(res => new RegisterSuccessAction({token: res.headers.get('authorization'), accountInfo: res.body})),
                catchError(error => of(new RegisterErrorAction(error))),
            ),
        ),
    );

    @Effect({dispatch: false})
    onRegisterSuccess$: Observable<RegActions> = this.actions$.pipe(
        ofType<RegisterSuccessAction>(RegisterTypes.RegisterSuccess),
        map(action => action.payload),
        tap((res: { token: string, accountInfo: IAccountInfo }) => {
            const token = res.token;
            const accountInfo = res.accountInfo;
            if (!token || !accountInfo) {
                console.log('cannot find token');
                console.log('token: ', token);
                console.log('accountInfo: ', accountInfo);
                return;
            }
            this.localStorage.setItem(StorageItem.Token, token.substring(7));
            this.localStorage.setItem(StorageItem.AccountInfo, JSON.stringify(accountInfo));
            this.router.navigate(['/']);
        }),
        switchMap(() => empty()),
    );

    @Effect()
    setAccountInfo$: Observable<Action> = this.actions$.pipe(
        ofType<SetAccountInfoAction>(RegisterTypes.SetAccountInfo),
        map(() => {
            const info: string = this.localStorage.getItem(StorageItem.AccountInfo);
            if (!info) throw new Error('No account info found!');
            const accountInfo: IAccountInfo = <IAccountInfo>JSON.parse(info);
            return new SetAccountInfoSuccessAction(accountInfo);
        }),
        catchError(e => of(new SetAccountInfoErrorAction(e))),
    );

    @Effect({dispatch: false})
    onError$: Observable<RegActions> = this.actions$.pipe(
        ofType<RegisterErrorAction>(RegisterTypes.RegisterError),
        map(action => action.payload),
        tap(e => {
            console.error(e);
        }),
    );

    constructor(
        private actions$: Actions,
        private registerService: RegisterService,
        private localStorage: LocalStorageService,
        private router: Router,
        private dialog: MatDialog,
    ) {
    }
}
