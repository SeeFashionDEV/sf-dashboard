import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { InstallationComponent } from './installation.component';

@NgModule({
    imports: [
        CommonModule,
        CoreModule,
        ReactiveFormsModule,
    ],
    exports: [],
    declarations: [
        InstallationComponent,
    ],
    providers: [

    ],
})
export class InstallationModule { }
