import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IMapRegion } from '../../../../../models/interfaces/map-region';
import { MapRegion } from '../../../../../models/enums/map-region';
import { MapColor } from '../../../../../models/enums/map-colors';

@Component({
    selector: 'db-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
})
export class DashboardMapComponent implements OnInit {
    @Output() select: EventEmitter<MapRegion> = new EventEmitter();
    regions: IMapRegion[];

    ngOnInit(): void {
        this.regions = [
            {
                region: MapRegion.EastCoast,
                label: 'East Coast',
                color: MapColor.Orange,
                x: 22,
                y: 33,
                size: 7,
                height: 60,
            },
            {
                region: MapRegion.WestCoast,
                label: 'West Coast',
                color: MapColor.Green,
                x: 8,
                y: 20,
                size: 9,
                height: 70,
            },
            {
                region: MapRegion.SouthAmerica,
                label: 'South America',
                color: MapColor.Blue,
                x: 29,
                y: 63,
                size: 7,
                height: 25,
            },
            {
                region: MapRegion.NorthAfrica,
                label: 'North Africa',
                color: MapColor.Aqua,
                x: 42,
                y: 42,
                size: 7,
                height: 50,
            },
            {
                region: MapRegion.SouthAfrica,
                label: 'South Africa',
                color: MapColor.Beige,
                x: 50,
                y: 71,
                size: 6,
                height: 14,
            },
            {
                region: MapRegion.UnitedKingdom,
                label: 'United Kingdom',
                color: MapColor.Red,
                x: 45,
                y: 31,
                size: 4,
                height: 62,
            },
            {
                region: MapRegion.MoscowArea,
                label: 'Moscow Area',
                color: MapColor.Violet,
                x: 59,
                y: 30,
                size: 4,
                height: 54,
            },
            {
                region: MapRegion.IndiaAndOceania,
                label: 'India / Oceania',
                color: MapColor.Yellow,
                x: 67,
                y: 45,
                size: 6,
                height: 32,
            },
            {
                region: MapRegion.Japan,
                label: 'Japan',
                color: MapColor.DarkGreen,
                x: 79,
                y: 36,
                size: 7,
                height: 56,
            },
            {
                region: MapRegion.Australia,
                label: 'Australia',
                color: MapColor.DarkBlue,
                x: 84,
                y: 74,
                size: 4,
                height: 19,
            },
        ];
    }

    onSelect(region: MapRegion): void {
        this.select.emit(region);
    }

}
