import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ScrollService } from '../../../../services/scroll.service';
import { ChangeFilterStateAction } from '../../store/filter/filter.action';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store';
import { FilterRange } from '../../../../models/enums/filter-range';

@Component({
    selector: 'app-performance',
    templateUrl: './performance.component.html',
    styleUrls: ['./performance.component.scss'],
})
export class PerformanceComponent implements OnInit {
    constructor(
        public ref: ElementRef,
        private scrollService: ScrollService,
        private store: Store<AppState>,
    ) {
    }

    ngOnInit(): void {
        this.scrollService.setContainer(this.ref);
        this.store.dispatch(new ChangeFilterStateAction({
            range: FilterRange.MONTH,
        }));
    }

}
