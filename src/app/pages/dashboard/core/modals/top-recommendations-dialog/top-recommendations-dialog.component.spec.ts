import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopRecommendationsDialogComponent } from './top-recommendations-dialog.component';

describe('TopRecommendationsDialogComponent', () => {
  let component: TopRecommendationsDialogComponent;
  let fixture: ComponentFixture<TopRecommendationsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopRecommendationsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopRecommendationsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
