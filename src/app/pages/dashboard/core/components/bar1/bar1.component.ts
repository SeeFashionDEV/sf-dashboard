import { Component, Input } from '@angular/core';
import { DataType } from '../../../../../models/enums/data-type';

@Component({
    selector: 'db-bar1',
    templateUrl: './bar1.component.html',
    styleUrls: ['./bar1.component.scss'],
})
export class DashboardBar1Component {
    DataType = DataType;
    @Input() value: number;
    @Input() total: number;
    @Input() color: string = 'blue';
    @Input() progress: number;
    @Input() dataType: DataType = DataType.NUMERIC;

}
