import { Injectable, EventEmitter, ElementRef } from '@angular/core';

export class ScrollTo {
    position: ScrollToPosition;
    behavior: ScrollToBehavior;

    constructor(position?: ScrollToPosition, behavior?: ScrollToBehavior) {
        this.position = position || ScrollToPosition.Start;
        this.behavior = behavior || ScrollToBehavior.Smooth;
    }
}

export enum ScrollToPosition {
    Start = 'start',
    End = 'end',
    Center = 'center',
}

export enum ScrollToBehavior {
    Smooth = 'smooth',
    Instant = 'instant',
}

@Injectable()
export class ScrollService {
    ref: ElementRef;
    scrollEvent: EventEmitter<ScrollTo> = new EventEmitter();

    constructor(
    ) { }

    setContainer(ref: ElementRef): void {
        this.ref = ref;
    }

    scrollTo(direction: ScrollToPosition, behavior: ScrollToBehavior): void {
        if (this.ref) {
            this.ref.nativeElement.scrollIntoView({ block: direction, behavior: behavior });
            return;
        }
        this.scrollEvent.next(new ScrollTo(direction, behavior));
    }

    smoothScrollTop(el?: HTMLElement): void {
        if (el) {
            el.scrollIntoView({ block: ScrollToPosition.Start, behavior: ScrollToBehavior.Smooth });
            return;
        }
        this.scrollEvent.next(new ScrollTo(ScrollToPosition.Start, ScrollToBehavior.Smooth));
    }

    smoothScrollBottom(el?: HTMLElement): void {
        if (el) {
            el.scrollIntoView({ block: ScrollToPosition.End, behavior: ScrollToBehavior.Smooth });
            return;
        }
        this.scrollEvent.next(new ScrollTo(ScrollToPosition.End, ScrollToBehavior.Smooth));
    }

    smoothScrollCenter(el?: HTMLElement): void {
        if (el) {
            el.scrollIntoView({ block: ScrollToPosition.Center, behavior: ScrollToBehavior.Smooth });
            return;
        }
        this.scrollEvent.next(new ScrollTo(ScrollToPosition.Center, ScrollToBehavior.Smooth));
    }

    instantScrollTop(el?: HTMLElement): void {
        if (el) {
            el.scrollIntoView({ block: ScrollToPosition.Start, behavior: ScrollToBehavior.Instant });
            return;
        }
        this.scrollEvent.next(new ScrollTo(ScrollToPosition.Start, ScrollToBehavior.Instant));
    }

    instantScrollCenter(el?: HTMLElement): void {
        if (el) {
            el.scrollIntoView({ block: ScrollToPosition.Center, behavior: ScrollToBehavior.Instant });
            return;
        }
        this.scrollEvent.next(new ScrollTo(ScrollToPosition.Center, ScrollToBehavior.Instant));
    }

    instantScrollBottom(el?: HTMLElement): void {
        if (el) {
            el.scrollIntoView({ block: ScrollToPosition.End, behavior: ScrollToBehavior.Instant });
            return;
        }
        this.scrollEvent.next(new ScrollTo(ScrollToPosition.End, ScrollToBehavior.Instant));
    }

    get scrollTop(): number {
        return this.ref.nativeElement.scrollTop;
    }

    disableScroll(): void {
        document.documentElement.classList.remove('scrollable');
    }

    enableScroll(): void {
        document.documentElement.classList.add('scrollable');
    }
}