export enum MapColor {
    Orange = 'f49d13',
    Blue = '1388f4',
    Green = '13f44d',
    Red = 'f70b0b',
    Beige = 'f9d596',
    Yellow = 'f3ed3c',
    Aqua = '3cf3b0',
    Violet = 'c91ff4',
    DarkGreen = '107425',
    DarkBlue = '262498',
}
