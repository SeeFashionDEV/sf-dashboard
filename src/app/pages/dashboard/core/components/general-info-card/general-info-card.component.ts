import { Component, Input, HostListener, ViewChild, ElementRef } from '@angular/core';
import { fadeInOut, fadeIn } from '../../../../../core/animations/fade.animation';
import { IGeneralInfoData } from '../../../../../models/interfaces/general-info-data';
import { DataType } from '../../../../../models/enums/data-type';

@Component({
    selector: 'db-general-info-card',
    templateUrl: './general-info-card.component.html',
    styleUrls: ['./general-info-card.component.scss'],
    animations: [fadeIn],
})
export class DashboardGeneralInfoCardComponent {
    DataType = DataType;
    @Input() data: IGeneralInfoData;
    @Input() currency: string = 'GBP';
    @ViewChild('helpMessageRef') helpMessageRef: ElementRef;

    isHelpMessageVisible: boolean = false;

    showHelpMessage(e: MouseEvent): void {
        e.preventDefault();
        e.stopPropagation();
        this.isHelpMessageVisible = true;
    }

    @HostListener('window:click', ['$event'])
    onKeyUp(e: MouseEvent): void {
        if (!this.helpMessageRef ||
            this.helpMessageRef.nativeElement.contains(e.target) ||
            !this.helpMessageRef) return;
        this.isHelpMessageVisible = false;
    }
}
