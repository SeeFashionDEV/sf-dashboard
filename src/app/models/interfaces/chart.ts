export interface IChartData {
    datasets: IChartDataset[];
    labels: string[];
}

export interface IChartDataset {
    data: any[];
    backgroundColor: string[];
}
