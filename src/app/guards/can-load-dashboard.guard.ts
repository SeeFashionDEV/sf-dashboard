import { Injectable } from '@angular/core';
import { CanLoad, Route, Router } from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';
import { StorageItem } from '../models/enums/storage-item';

@Injectable()
export class CanLoadDashboardGuard implements CanLoad {
    constructor(
        private storage: LocalStorageService,
        private router: Router,
    ) { }

    canLoad(): boolean {
        const token = this.storage.getItem(StorageItem.Token);
        if (!token) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
}