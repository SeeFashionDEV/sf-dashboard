import { Action } from '@ngrx/store';
import { MapRegion } from '../../../../models/enums/map-region';
import { IOpportunities } from '../../../../models/interfaces/opportunities';

export enum OpportunitiesTypes {
    LoadData = '[OPPORTUNITIES] Load data [...]',
    LoadDataSuccess = '[OPPORTUNITIES] Load data [SUCCESS]',
    LoadDataError = '[OPPORTUNITIES] Load data [ERROR]',
    SelectRegion = '[OPPORTUNITIES] Select region [...]',
    SelectRegionSuccess = '[OPPORTUNITIES] Select region [SUCCESS]',
    SelectRegionError = '[OPPORTUNITIES] Select region [ERROR]',
}

// LOAD DATA
export class LoadOpportunitiesDataAction implements Action {
    readonly type = OpportunitiesTypes.LoadData;
}

export class LoadOpportunitiesDataSuccessAction implements Action {
    readonly type = OpportunitiesTypes.LoadDataSuccess;

    constructor(public payload: IOpportunities) { }
}

export class LoadOpportunitiesDataErrorAction implements Action {
    readonly type = OpportunitiesTypes.LoadDataError;

    constructor(public payload: any) { }
}

// SELECT REGION
export class SelectRegionAction implements Action {
    readonly type = OpportunitiesTypes.SelectRegion;

    constructor(public payload: MapRegion) { }
}

export type OpportunitiesActions
    = LoadOpportunitiesDataAction
    | LoadOpportunitiesDataSuccessAction
    | LoadOpportunitiesDataErrorAction
    | SelectRegionAction
    ;