import { Component, OnInit } from '@angular/core';
import { MapRegion } from '../../../../../../models/enums/map-region';
import { DataType } from '../../../../../../models/enums/data-type';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../store';
import { Observable } from 'rxjs';
import { getOpportunitiesState } from '../../../../store';
import { OpenTopRecommendationDialogAction } from '../../../../store/recommendations/recommendations.action';
import { SelectRegionAction } from '../../../../store/opportunities/opportunities.action';
import { IOpportunities, IOpportunitiesTotalRevenue } from '../../../../../../models/interfaces/opportunities';
import { fadeIn } from '../../../../../../core/animations/fade.animation';

@Component({
    selector: 'db-opportunities',
    templateUrl: './opportunities.component.html',
    styleUrls: ['./opportunities.component.scss'],
    animations: [fadeIn],
})
export class DashboardOpportunitiesComponent implements OnInit {

    data$: Observable<IOpportunities>;

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.data$ = this.store.select(getOpportunitiesState);
    }

    onMapSelect(region: MapRegion): void {
        this.store.dispatch(new SelectRegionAction(region));
    }

    getTotal(data: IOpportunitiesTotalRevenue): number {
        if (!data) return 0;
        return data.bar1.seeFashion.value + data.bar1.standard.value;
    }

    showItem(item): void {
        this.store.dispatch(new OpenTopRecommendationDialogAction(item));
    }
}
