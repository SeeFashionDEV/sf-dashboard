#!/usr/bin/env bash
cd /home/ec2-user/server
# active profile means which application-*.properties we pick,
# "> /dev/null" means throw away the output (usually logback init stuff) and "2>&1" means redirect STDERR to STDOUT which we mentioned to throw away
# & at the end means run the command in the background
sudo service nginx start
