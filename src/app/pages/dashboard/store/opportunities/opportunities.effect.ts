import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, empty } from 'rxjs';
import {
    OpportunitiesActions, LoadOpportunitiesDataAction, OpportunitiesTypes,
    LoadOpportunitiesDataSuccessAction,
    SelectRegionAction,
} from './opportunities.action';
import { AppState } from '../../../../store/index';
import { Store } from '@ngrx/store';
import { withLatestFrom, switchMap } from 'rxjs/operators';
import { FilterRange } from '../../../../models/enums/filter-range';
import { DataType } from '../../../../models/enums/data-type';
import { FilterActions, FilterTypes } from '../filter/filter.action';
import { IRecommendation } from '../../../../models/interfaces/recomendation';
import { MapRegion } from '../../../../models/enums/map-region';
import { IOpportunitiesTotalRevenue } from '../../../../models/interfaces/opportunities';

@Injectable()
export class OpportunitiesEffects {

    // @Effect()
    // loadData$: Observable<OpportunitiesActions> = this.actions$.pipe(
    //     ofType<LoadOpportunitiesDataAction>(OpportunitiesTypes.LoadData),
    //     withLatestFrom(this.store.select(getFilterState), this.store.select(getActiveRegion)),
    //     switchMap(([, filter, region]) => {
    //         if (!filter) return empty();
    //         const totalRevenue = this.getTotalRevenueData(filter.range, region);
    //         const mostPopular = this.getmostPopularData(filter.range, region);
    //         const missedOpportunities = this.getmissedOpportunitiesData(filter.range, region);
    //         return of(new LoadOpportunitiesDataSuccessAction({
    //             totalRevenue: totalRevenue,
    //             mostPopular: mostPopular,
    //             missedOpportunities: missedOpportunities,
    //         }));
    //     }),
    // );

    @Effect()
    reloadData$: Observable<OpportunitiesActions> = this.actions$.pipe(
        ofType<FilterActions>(FilterTypes.ChangeFilterState),
        switchMap(() => of(new LoadOpportunitiesDataAction())),
    );

    @Effect()
    changeRegion$: Observable<OpportunitiesActions> = this.actions$.pipe(
        ofType<SelectRegionAction>(OpportunitiesTypes.SelectRegion),
        switchMap(() => of(new LoadOpportunitiesDataAction())),
    );

    constructor(
        private actions$: Actions,
        private store: Store<AppState>,
    ) { }

    getTotalRevenueData(range: FilterRange, region: MapRegion): IOpportunitiesTotalRevenue {
        if (range == null) return;
        return {
            dataType: DataType.CURRENCY,
            bar1: {
                standard: {
                    value: this.getRandomInteger(100, 1500),
                    kpi: this.getRandomInteger(-10, 10),
                },
                seeFashion: {
                    value: this.getRandomInteger(100, 1500),
                    kpi: this.getRandomInteger(-10, 10),
                },
            },
        };
    }

    getmostPopularData(range: FilterRange, region: MapRegion): IRecommendation[] {
        if (range == null) return;
        return this.getRandomItems(4);
    }

    getmissedOpportunitiesData(range: FilterRange, region: MapRegion): IRecommendation[] {
        if (range == null) return;
        return this.getRandomItems(8);
    }

    getRandomItems(n): IRecommendation[] {
        const result = new Array();

        if (n > itemsPool.length)
            throw new RangeError('etRandom: more elements taken than available');
        while (result.length < n) {
            const randIndex = Math.floor(Math.random() * itemsPool.length);
            const randItem = itemsPool[randIndex];
            if (result.map(res => res.productImage).indexOf(randItem.productImage) === -1) {
                result.push(randItem);
            }
        }
        return result;
    }

    getRandomInteger(min, max): number {
        const rand = min - 0.5 + Math.random() * (max - min + 1);
        return Math.round(rand);
    }
}

// tslint:disable:max-line-length
const itemsPool: IRecommendation[] = [
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8128,
        itemsOnStock: 0,
        noOfClick: 0,
        productId: 386881323051,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/aa3f38ee275079e545849a62f2dbec58_d6ac5cab-eac6-4569-affc-7c5e241d4fc6.jpg?v=1512998029',
        productTitle: 'Bahama Mama Maxi Dress',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/a00613c0dad72815e3cfcf5b3993c7ad.jpg?v=1512615733',
        productTitle: 'Long Sleeve Women Sleepwear',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8121,
        itemsOnStock: 3,
        noOfClick: 1,
        productId: 383094751275,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/d6d7ce8594d524d0f961a26b33b6ba74.jpg?v=1510869291',
        productTitle: 'Essential Windbreaker (White)',
        shopId: 112,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/4a4a57d152a0494be9601dbf2dd43bfc_05a8cfd1-c886-4bea-abcf-f40ea14c840a.jpg?v=1515673622',
        productTitle: 'Black Floral Shirt Dress Ex-branded',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8128,
        itemsOnStock: 0,
        noOfClick: 0,
        productId: 386881323051,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/b195db0bd8823c18f8ad96edf71726ce.jpg?v=1513462717',
        productTitle: 'Sequin Obsession Playsuit',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.433Z',
        id: 8157,
        itemsOnStock: 0,
        noOfClick: 2,
        productId: 383101960235,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/49eb9b9e104d345b79202847ab80535c.jpg?v=1510868643',
        productTitle: 'Purple striped white shirt',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/b195db0bd8823c18f8ad96edf71726ce.jpg?v=1513462717',
        productTitle: 'Celestial Off Shoulder Rompe',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://mia.aopcdn.com/navigation/1516618772941.jpg?t=1518090875',
        productTitle: 'Long Sleeve Women Sleepwear',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8121,
        itemsOnStock: 3,
        noOfClick: 1,
        productId: 383094751275,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/d6d7ce8594d524d0f961a26b33b6ba74.jpg?v=1510869291',
        productTitle: 'Essential Windbreaker (White)',
        shopId: 112,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/4a4a57d152a0494be9601dbf2dd43bfc_05a8cfd1-c886-4bea-abcf-f40ea14c840a.jpg?v=1515673622',
        productTitle: 'Black Floral Shirt Dress Ex-branded',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8128,
        itemsOnStock: 0,
        noOfClick: 0,
        productId: 386881323051,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/b195db0bd8823c18f8ad96edf71726ce.jpg?v=1513462717',
        productTitle: 'Sequin Obsession Playsuit',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.433Z',
        id: 8157,
        itemsOnStock: 0,
        noOfClick: 2,
        productId: 383101960235,
        productImage: 'https://ae01.alicdn.com/kf/HTB1E.95mjihSKJjy0Ffq6zGzFXam/New-Women-s-Vintage-Jeans-Skirt-fashionable-elegant-Autumn-winter-long-denim-skirt-with-irregular-hem.jpg_640x640.jpg',
        productTitle: 'Purple striped white shirt',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://img2.wbstatic.net/big/new/1880000/1880574-1.jpg',
        productTitle: 'Celestial Off Shoulder Rompe',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/a00613c0dad72815e3cfcf5b3993c7ad.jpg?v=1512615733',
        productTitle: 'Long Sleeve Women Sleepwear',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8121,
        itemsOnStock: 3,
        noOfClick: 1,
        productId: 383094751275,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/d6d7ce8594d524d0f961a26b33b6ba74.jpg?v=1510869291',
        productTitle: 'Essential Windbreaker (White)',
        shopId: 112,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/4a4a57d152a0494be9601dbf2dd43bfc_05a8cfd1-c886-4bea-abcf-f40ea14c840a.jpg?v=1515673622',
        productTitle: 'Black Floral Shirt Dress Ex-branded',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8128,
        itemsOnStock: 0,
        noOfClick: 0,
        productId: 386881323051,
        productImage: 'https://cdn.shopify.com/s/files/1/2552/3988/products/b195db0bd8823c18f8ad96edf71726ce.jpg?v=1513462717',
        productTitle: 'Sequin Obsession Playsuit',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.433Z',
        id: 8157,
        itemsOnStock: 0,
        noOfClick: 2,
        productId: 383101960235,
        productImage: 'https://d166chel5lrjm5.cloudfront.net/images/detailed/47/nolitablack3.jpg',
        productTitle: 'Purple striped white shirt',
        shopId: 25523988,
        soldItems: null,
    },
    {
        createdAt: '2018-04-13T00:11:20.432Z',
        id: 8107,
        itemsOnStock: 0,
        noOfClick: 1,
        productId: 387286728747,
        productImage: 'https://images-eu.ssl-images-amazon.com/images/G/02/uk-sports/2016_Flips/AW16/Photoshoot/Voodoo/Lifestyle_Female_520x720',
        productTitle: 'Celestial Off Shoulder Rompe',
        shopId: 25523988,
        soldItems: null,
    },
];
