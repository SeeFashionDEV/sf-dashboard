import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { DashboardComponent } from './dashboard.component';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule } from '@angular/router';
import { DASHBOARD_ROUTES } from './dashboard.routing';
import { NavigationModule } from './navigation/navigation.module';
import { DASHBOARD_PAGES } from './pages';
import { StoreModule } from '@ngrx/store';
import { dbReducers } from './store';
import { EffectsModule } from '@ngrx/effects';
import { DashboardCoreModule } from './core/core.module';
import { Recommendationsffects } from './store/recommendations/recommendations.effect';
import { FilterEffects } from './store/filter/filter.effect';
import { OpportunitiesEffects } from './store/opportunities/opportunities.effect';
import { RecosEffects } from './store/recos/recos.effect';

@NgModule({
    imports: [
        CommonModule,
        CoreModule,
        LayoutModule, RouterModule.forChild(DASHBOARD_ROUTES),
        NavigationModule,
        DashboardCoreModule,
        ...DASHBOARD_PAGES,
        StoreModule.forFeature('dashboard', dbReducers),
        EffectsModule.forFeature([
            FilterEffects,
            Recommendationsffects,
            OpportunitiesEffects,
            RecosEffects,
        ]),
    ],
    exports: [],
    declarations: [
        DashboardComponent,
    ],
    providers: [
    ],
})
export class DashboardModule { }
