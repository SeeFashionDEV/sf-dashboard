import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../store/index';
import { getVisitorsAndSalesState } from '../../../../store';
import {
    IAddToCartData, ISalesData,
    IVisitorsData,
} from '../../../../../../models/interfaces/visitors-and-sales';
import { map, skipWhile } from 'rxjs/operators';

@Component({
    selector: 'db-visitors-and-sales',
    templateUrl: './visitors-and-sales.component.html',
    styleUrls: ['./visitors-and-sales.component.scss'],
})
export class DashboardVisitorsAndSalesComponent implements OnInit {
    visitors$: Observable<IVisitorsData>;
    addToCart$: Observable<IAddToCartData>;
    sales$: Observable<ISalesData>;

    constructor(
        private store: Store<AppState>,
    ) {
    }

    ngOnInit(): void {
        const data$ = this.store.select(getVisitorsAndSalesState).pipe(skipWhile(s => !s));
        this.visitors$ = data$.pipe(map(d => d.visitors), skipWhile(s => !s));
        this.addToCart$ = data$.pipe(map(d => d.addToCard), skipWhile(s => !s));
        this.sales$ = data$.pipe(map(d => d.sales), skipWhile(s => !s));
    }

}
