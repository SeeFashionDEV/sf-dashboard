import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardGeneralInfoComponent } from './general-info.component';

describe('GeneralInfoComponent', () => {
    let component: DashboardGeneralInfoComponent;
    let fixture: ComponentFixture<DashboardGeneralInfoComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardGeneralInfoComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardGeneralInfoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
