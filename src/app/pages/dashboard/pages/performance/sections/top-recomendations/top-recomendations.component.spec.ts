import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTopRecomendationsComponent } from './top-recomendations.component';

describe('TopRecomendationsComponent', () => {
    let component: DashboardTopRecomendationsComponent;
    let fixture: ComponentFixture<DashboardTopRecomendationsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardTopRecomendationsComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardTopRecomendationsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
