import { trigger, state, style, animate, transition } from '@angular/animations';

export const fadeOut = trigger('fadeOut', [
    state('*', style({ opacity: 1 })),
    state('void', style({ opacity: 0 })),
    transition(':leave', [
        style({ opacity: 1 }),
        animate('0.2s ease-in-out', style({ opacity: 0 })),
    ]),
]);

export const fadeIn = trigger('fadeIn', [
    state('void', style({ opacity: 0 })),
    state('*', style({ opacity: 1 })),
    transition(':enter', [
        style({ opacity: 0 }),
        animate('0.2s linear', style({ opacity: 1 })),
    ]),
]);

export const fadeInOut = trigger('fadeInOut', [
    state('void', style({ opacity: 0 })),
    state('*', style({ opacity: 1 })),
    transition(':enter', [
        style({ opacity: 0 }),
        animate('0.2s ease-in-out', style({ opacity: 1 })),
    ]),
    transition(':leave', [
        style({ opacity: 1 }),
        animate('0.2s ease-in-out', style({ opacity: 0 })),
    ]),
]);