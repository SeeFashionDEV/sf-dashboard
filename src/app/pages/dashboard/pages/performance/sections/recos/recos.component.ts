import { Component, OnInit } from '@angular/core';
import { DataType } from '../../../../../../models/enums/data-type';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getRecosState } from '../../../../store/index';
import { AppState } from '../../../../../../store/index';
import { IRecos } from '../../../../../../models/interfaces/recos';

@Component({
    selector: 'app-recos',
    templateUrl: './recos.component.html',
    styleUrls: ['./recos.component.scss'],
})
export class DashboardRecosComponent implements OnInit {
    data$: Observable<IRecos>;

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.data$ = this.store.select(getRecosState);
    }

}
