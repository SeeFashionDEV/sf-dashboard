import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/index';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterAction } from '../../store/register/register.action';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
    form: FormGroup;

    constructor(
        private store: Store<AppState>,
        private fb: FormBuilder,
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            shopDomain: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8)]],
            password2: ['', [Validators.required, Validators.minLength(8)]],
        });
    }

    onRegister(): void {
        if (this.form.invalid) return;
        const creds = this.form.value;
        this.store.dispatch(new RegisterAction(creds));
    }
}
