import { NgModule } from '@angular/core';
import { CORE_COMPONENTS } from './components';
import { MaterialModule } from './modules/material.module';

@NgModule({
    imports: [
        MaterialModule,
    ],
    exports: [
        MaterialModule,
        ...CORE_COMPONENTS,
    ],
    declarations: [
        ...CORE_COMPONENTS,
    ],
    entryComponents: [
    ],
    providers: [],
})
export class CoreModule { }
