import { DashboardCardHeaderComponent } from './card-header/card-header.component';
import { DashboardGeneralInfoCardComponent } from './general-info-card/general-info-card.component';
import { ValueDynamicComponent } from './value-dynamic/value-dynamic.component';
import { DashboardBar1Component } from './bar1/bar1.component';
import { DashboardBar2Component } from './bar2/bar2.component';
import { DashboardBlocksComponent } from './blocks/blocks.component';
import { DashboardInfoBlockComponent } from './info-block/info-block.component';
import { DashboardMapComponent } from './map/map.component';
import { DashboardMapRegionComponent } from './map-region/map-region.component';
import { DashboardLinearChartComponent } from './chart/linear-chart/linear-chart.component';
import { FastScrollComponent } from './fast-scroll/fast-scroll.component';

export const DASHBOARD_CORE_COMPONENTS = [
    DashboardCardHeaderComponent,
    DashboardGeneralInfoCardComponent,
    ValueDynamicComponent,
    DashboardBar1Component,
    DashboardBar2Component,
    DashboardBlocksComponent,
    DashboardInfoBlockComponent,
    DashboardMapComponent,
    DashboardMapRegionComponent,
    DashboardLinearChartComponent,
    FastScrollComponent,
];