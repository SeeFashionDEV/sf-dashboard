import { IData } from './data';
import { IRecommendation } from './recomendation';
import { DataType } from '../enums/data-type';

export interface IOpportunitiesTotalRevenue {
    dataType: DataType;
    bar1: {
        standard: IData;
        seeFashion: IData;
    };
}

export interface IOpportunities {
    totalRevenue: IOpportunitiesTotalRevenue;
    mostPopular: IRecommendation[];
    missedOpportunities: IRecommendation[];
}