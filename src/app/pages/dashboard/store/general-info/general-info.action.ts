import { Action } from '@ngrx/store';
import { IGeneralInfo } from '../../../../models/interfaces/general-info';

export enum GeneralInfoTypes {
    LoadData = '[GENERAL-INFO] Load data',
}

// LOAD DATA
export class LoadGeneralInfoDataAction implements Action {
    readonly type = GeneralInfoTypes.LoadData;

    constructor(public payload: IGeneralInfo) {
    }
}

export type GeneralInfoActions
    = LoadGeneralInfoDataAction
    ;
