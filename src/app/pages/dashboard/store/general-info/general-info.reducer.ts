import { GeneralInfoTypes, GeneralInfoActions } from './general-info.action';
import { IGeneralInfoData } from '../../../../models/interfaces/general-info-data';
import { IGeneralInfo } from '../../../../models/interfaces/general-info';

export class GeneralInfoState implements IGeneralInfo {
    currency: string;
    totalRevenue: IGeneralInfoData;
    averageOrder: IGeneralInfoData;
    conversionRate: IGeneralInfoData;
    productViews: IGeneralInfoData;
    revenuesChartData: any;
}

export function reducer(state = new GeneralInfoState(), action: GeneralInfoActions): GeneralInfoState {
    switch (action.type) {
        case GeneralInfoTypes.LoadData:
            return action.payload;
        default: {
            return state;
        }
    }
}
