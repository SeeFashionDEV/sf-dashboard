import { Action } from '@ngrx/store';
import { IInstall } from '../../models/interfaces/IInstall';
import { Status } from '../../models/interfaces/status';

export enum InstallationTypes {
    Install = '[REG] Install [OK]',
    InstallSuccess = '[REG] Install [SUCCESS]',
    InstallError = '[REG] Install [ERROR]',
}

export class InstallationAction implements Action {
    readonly type = InstallationTypes.Install;

    constructor(public payload: IInstall) { }
}

export class InstallationSuccessAction implements Action {
    readonly type = InstallationTypes.InstallSuccess;

    constructor(public payload: {status: any}) { }
}


export class InstallationErrorAction implements Action {
    readonly type = InstallationTypes.InstallError;

    constructor(public payload: any) { }
}


export type InstallActions
    = InstallationAction
    | InstallationSuccessAction
    | InstallationErrorAction
    ;
