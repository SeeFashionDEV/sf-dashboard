import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { empty, Observable, of } from 'rxjs';
import { ChangeFilterStateAction, FilterTypes } from './filter.action';
import { catchError, map, switchMap } from 'rxjs/operators';
import { FilterRange } from '../../../../models/enums/filter-range';
import * as moment from 'moment';
import { HttpService } from '../../../../services/http.service';
import { IGeneralInfo } from '../../../../models/interfaces/general-info';
import { LoadGeneralInfoDataAction } from '../general-info/general-info.action';
import { DataType } from '../../../../models/enums/data-type';
import { Action } from '@ngrx/store';
import { IVisitorsAndSales } from '../../../../models/interfaces/visitors-and-sales';
import { IInfoBlockData } from '../../../../models/interfaces/info-block-data';
import { LoadVisitorsAndSalesDataAction } from '../visitors-and-sales/visitors-and-sales.action';

@Injectable()
export class FilterEffects {
    @Effect()
    filterChange: Observable<Action> = this.actions$.pipe(
        ofType<ChangeFilterStateAction>(FilterTypes.ChangeFilterState),
        map(action => action.payload),
        map((filterState) => {
            console.log(filterState);
            const range = filterState.range;
            let endDate: string;
            let startDate: string;
            if (range != null) {
                endDate = moment().format('YYYY-MM-DD');
                switch (range) {
                    case FilterRange.MONTH:
                        startDate = moment().subtract(1, 'months').format('YYYY-MM-DD');
                        break;
                    case FilterRange.WEEK:
                        startDate = moment().subtract(1, 'week').format('YYYY-MM-DD');
                        break;
                    case FilterRange.TODAY:
                        startDate = moment().subtract(1, 'day').format('YYYY-MM-DD');
                        break;
                    default:
                        endDate = undefined;
                        startDate = undefined;
                }
            } else if (filterState.custom) {
                console.log(filterState.custom);
                startDate = moment(filterState.custom.startDate).format('YYYY-MM-DD');
                endDate = moment(filterState.custom.endDate).format('YYYY-MM-DD');
            }
            return {startDate, endDate};
        }),
        switchMap((prop) => {
            return this.http.loadData(prop).pipe(
                catchError(err => of({err: err})),
            );
        }),
        switchMap(res => {
            if (res.err) {
                console.error(res.err);
                return empty();
            } else {
                console.log(res);
                return of(new LoadGeneralInfoDataAction(this._getGeneralInfo(res)),
                    new LoadVisitorsAndSalesDataAction(this._getVisitorsAndSales(res)));
            }
        }),
    );


    constructor(
        private actions$: Actions,
        private http: HttpService,
    ) {
    }

    private _getGeneralInfo(data: any): IGeneralInfo {
        const revenueData = data.data.revenue;
        const averageOrder = data.data.order_value;
        const conversionRate = data.data.conversion;
        const productViews = data.data.product_views;
        return {
            currency: data.currency,
            totalRevenue: {
                title: 'total revenue',
                // tslint:disable:max-line-length
                helpMessage: `The total revenue of all products sold on your website, including: \n
            - the proportion of total revenue generated through SeeFashion (recommendations) versus standard navigation on your website\n
            - The percentage change in the total revenue over the selected time period\n
            - the percentage change in the revenues generated through See Fashion versus standard navigation on your website over the selected time period.`,
                dataType: DataType.CURRENCY,
                total: {
                    value: revenueData.total_revenue,
                    kpi: revenueData.total_revenue_change,
                },
                standard: {
                    value: revenueData.non_sf_revenue,
                    kpi: revenueData.non_sf_revenue_change,
                },
                seeFashion: {
                    value: revenueData.sf_revenue,
                    kpi: revenueData.sf_percentage_revenue,
                },
            },
            averageOrder: {
                title: 'average order value',
                helpMessage: `Shows the average order value of a single purchase on your website, including:\n
            - The average contribution made by See Fashion recommendations to a single purchase versus the average order value without SeeFashion recommendations\n
            - The percentage change in the overall average order value over the selected time period\n
            - The percentage change in the average contribution made by See Fashion recommendations to a single purchase versus the average order value without SeeFashion recommendations over the selected time period`,
                dataType: DataType.CURRENCY,
                total: {
                    value: averageOrder.average_order_value,
                    kpi: averageOrder.avg_order_value_change,
                },
                standard: {
                    value: averageOrder.no_sf_average_order_value,
                    kpi: averageOrder.no_sf_order_value_change,
                },
                seeFashion: {
                    value: averageOrder.sf_average_order_value,
                    kpi: averageOrder.sf_order_value_change,
                },
            },
            conversionRate: {
                title: 'coversion rate',
                helpMessage: `The percentage of unique visitors that place an order on your website, including:\n
            - The percentage of unique visitors that placed an order with a SeeFashion recommended product (SeeFashion conversion rate) versus percentage of unique visitors that placed an order without a SeeFashion recommended product (Standard navigation conversion rate).\n
            - The percentage change in the overall conversion rate over the selected time period\n
            - The percentage change in the conversion rate with SeeFashion recommendations versus standard navigation on your website over the selected time period.`,
                dataType: DataType.PERCENTAGE,
                total: {
                    value: conversionRate.total_customer_conversion,
                    kpi: conversionRate.total_customer_conv_change,
                },
                standard: {
                    value: conversionRate.no_sf_conversion,
                    kpi: conversionRate.no_sf_conversion_change,
                },
                seeFashion: {
                    value: conversionRate.sf_conversion,
                    kpi: conversionRate.sf_customer_conv_change,
                },
            },
            productViews: {
                title: 'product views',
                helpMessage: `Total views for all products on your website, including:
            - The proportion of total products viewed via a SeeFashion recommendation versus standard navigation\n
            - The percentage change in total product views over a selected time period\n
            - The percentage change in total products viewed via a SeeFashion recommendation versus standard navigation over the selected time period`,
                dataType: DataType.NUMERIC,
                total: {
                    value: productViews.total_product_views,
                    kpi: productViews.total_prod_view_change,
                },
                standard: {
                    value: productViews.no_sf_product_views,
                    kpi: productViews.no_sf_product_views_change,
                },
                seeFashion: {
                    value: productViews.sf_product_views,
                    kpi: productViews.sf_prod_view_change,
                },
            },
            revenuesChartData: data.daily_sales,
        };
    }

    private _getVisitorsAndSales(data: any): IVisitorsAndSales {
        const vsData = data.data.visitors;
        const atcData = data.data.add_to_cart;
        const revenueData = data.data.revenue;
        const ovData = data.data.order_value;
        return {
            visitors: {
                uniqueVisitors: {
                    title: 'unique visitors',
                    dataType: DataType.NUMERIC,
                    value: vsData.total_unique_visitors,
                    kpi: vsData.total_unique_visitors_change,
                },
                numberOfProductsViewed: {
                    title: 'number of products viewed',
                    dataType: DataType.NUMERIC,
                    value: 9,
                    kpi: 0,
                    bar1: {
                        standard: {
                            value: vsData.average_non_sf_product_views_per_visitor,
                            kpi: vsData.average_non_sf_product_views_per_visitor_change,
                        },
                        seeFashion: {
                            value: vsData.average_sf_product_views_per_visitor,
                            kpi: vsData.average_sf_product_views_per_visitor_change,
                        },
                    },
                },
                averageTimePerProductView: {
                    title: 'average time spent per product view',
                    dataType: DataType.NUMERIC,
                    value: vsData.average_time_spent_per_product,
                    kpi: vsData.average_time_spent_per_product_change,
                    bar1: {
                        standard: {
                            value: vsData.average_time_spent_per_product_no_sf,
                            kpi: vsData.average_time_spent_per_product_no_sf_change,
                        },
                        seeFashion: {
                            value: vsData.average_time_spent_per_product_sf,
                            kpi: vsData.average_time_spent_per_product_sf_change,
                        },
                    },
                },
                numberOfRecomendations: {
                    title: 'number of recomendataions server per visitor',
                    dataType: DataType.NUMERIC,
                    value: vsData.average_recommendations_per_visitor,
                    kpi: vsData.average_recommendations_per_visitor_change,
                },
            },
            addToCard: {
                totalAddToCarts: {
                    title: 'total add to carts',
                    dataType: DataType.NUMERIC,
                    value: atcData.total_products_added_to_cart,
                    kpi: atcData.total_products_added_to_cart_change,
                },
                totalValueOfAddToCarts: {
                    title: 'total value of add to carts',
                    dataType: DataType.NUMERIC,
                    value: atcData.total_value_added_to_cart,
                    kpi: atcData.total_value_added_to_cart_change,
                    bar1: {
                        seeFashion: {
                            value: atcData.total_value_sf_added_to_cart,
                            kpi: atcData.total_value_sf_added_to_cart_change,
                        },
                        standard: {
                            value: atcData.total_value_no_sf_added_to_cart,
                            kpi: atcData.total_value_no_sf_added_to_cart_change,
                        },
                    },
                },
                averageNumberOfItemsPerVisitor: {
                    title: 'average cart value per visitor',
                    dataType: DataType.NUMERIC,
                    value: atcData.avg_cart_value_per_visitor,
                    kpi: atcData.avg_cart_value_per_visitor_change,
                    bar1: {
                        seeFashion: {
                            value: atcData.avg_cart_sf_value_per_visitor,
                            kpi: atcData.avg_cart_sf_value_per_visitor_change,
                        },
                        standard: {
                            value: atcData.avg_cart_no_sf_value_per_visitor,
                            kpi: atcData.avg_cart_no_sf_value_per_visitor_change,
                        },
                    },
                },
            },
            sales: {
                revenues: {
                    title: 'revenues',
                    dataType: DataType.NUMERIC,
                    value: revenueData.total_revenue,
                    kpi: revenueData.total_revenue_change,
                },
                revenuePerVisit: {
                    title: 'revenue per visit',
                    dataType: DataType.NUMERIC,
                    value: ovData.revenue_per_visit,
                },
                revenuePerProductView: {
                    title: 'revenue per product view',
                    dataType: DataType.NUMERIC,
                    value: ovData.revenue_per_product_view,
                    kpi: ovData.revenue_per_product_view_change,
                },
                averageOrderSize: {
                    title: 'average order size',
                    dataType: DataType.NUMERIC,
                    value: ovData.average_order_value,
                    kpi: ovData.avg_order_value_change,
                    bar1: {
                        seeFashion: {
                            value: ovData.sf_average_order_value,
                            kpi: ovData.sf_order_value_change,
                        },
                        standard: {
                            value: ovData.no_sf_average_order_value,
                            kpi: ovData.no_sf_order_value_change,
                        },
                    },
                },
            },
        };
    }
}
