import { Component, OnInit, ViewChild, ElementRef, SimpleChanges, OnChanges, Input, OnDestroy } from '@angular/core';
import { FilterRange } from '../../../../../../models/enums/filter-range';
import * as moment from 'moment';
import { AppState } from '../../../../../../store/index';
import { Store } from '@ngrx/store';
import { getGeneralInfoState } from '../../../../store';
import { Subscription } from 'rxjs';
import { CurrencyPipe } from '@angular/common';

declare var Chart: any;

@Component({
    selector: 'db-revenues-chart',
    templateUrl: './revenues-chart.component.html',
    styleUrls: ['./revenues-chart.component.scss'],
})

export class DashboardRevenuesChartComponent implements OnInit, OnChanges, OnDestroy {
    @Input() range: FilterRange = FilterRange.WEEK;
    @ViewChild('canvas') canvas: ElementRef;
    labels;
    sub: Subscription;
    chart: any;
    data = [{
        data: Array.from(this.getRange()),
        label: 'See fashion',
        fill: false,
        borderColor: [
            'rgba(0, 167, 116, 1)',
        ],
        borderWidth: 2,
    },
        {
            data: Array.from(this.getRange()),
            label: 'Standart',
            fill: false,
            borderColor: [
                'rgba(33, 47, 100, 1)',
            ],
            borderWidth: 2,
        }];

    options = {
        legend: {
            display: false,
        },
        tooltips: {
            callbacks: {
                label: function (tooltipItems, data): any {
                    return `${data.datasets[tooltipItems.datasetIndex].label}: ${tooltipItems.yLabel} £`;
                },
            },

        },
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: (value: string | number) => {
                        return `£${value}`;
                    },
                },
            }],
            xAxes: [{
                position: 'top',
                type: 'time',
                // time for week
                time: {},
            }],
        },
    };

    constructor(
        private store: Store<AppState>,
    ) {
    }

    ngOnInit(): void {
        this.sub = this.store.select(getGeneralInfoState).subscribe(el => {
            const currencyTransformer = new CurrencyPipe('en');
            this.options.tooltips.callbacks.label = function (tooltipItems, data): any {
                const val = parseInt(tooltipItems.yLabel);
                const transformedVal = currencyTransformer.transform(val, el.currency, 'symbol-narrow', '1.0-0');
                return `${data.datasets[tooltipItems.datasetIndex].label}: ${transformedVal}`;
            };
            this.options.scales.yAxes[0].ticks.callback = (value: string | number) => {
                return `${currencyTransformer.transform(value, el.currency, 'symbol-narrow', '1.0-0')}`;
            };
            const chartData = el.revenuesChartData;
            if (!chartData || !chartData.length) return;
            this.data[0].data = chartData.map(d => {
                return {
                    x: d.day,
                    y: d.sf_sales,
                };
            });
            this.data[1].data = chartData.map(d => {
                return {
                    x: d.day,
                    y: d.no_sf_sales,
                };
            });
            console.log(this.data);

            let axisType;
            switch (chartData.length) {
                case 7:
                    this.labels = this.getDateRande(7);
                    this.options.scales.xAxes[0].time = {
                        tooltipFormat: 'MMM Do YY',
                        unit: 'day',
                        displayFormats: {
                            day: 'dddd',
                        },
                    };
                    break;
                case 30:
                    axisType = 'month';
                    this.labels = this.getDateRande(30);
                    this.options.scales.xAxes[0].time = {
                        tooltipFormat: 'MMM Do YY',
                        unit: 'day',
                        displayFormats: {
                            millisecond: 'MMM DD',
                            second: 'MMM DD',
                            minute: 'MMM DD',
                            hour: 'MMM DD',
                            day: 'MMM DD',
                        },
                    };
                    break;
                case 1:
                    this.labels = this.getDateRande(2);
                    this.options.scales.xAxes[0].time = {
                        tooltipFormat: 'MMM Do YY',
                        unit: 'hour',
                        displayFormats: {
                            millisecond: 'MMM DD',
                            second: 'MMM DD',
                            minute: 'MMM DD',
                        },
                    };
                    break;
                default:
                    this.labels = this.getDateRande(chartData.length);
                    this.options.scales.xAxes[0].time = {
                        tooltipFormat: 'MMM Do YY',
                        unit: this.getUnit(chartData.length),
                    };
                    break;
            }
            this.setChart();
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
    }

    ngOnDestroy(): void {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    getRange(): number[] {
        const arr = [];
        for (let i = 0; i < 30; i++) {
            arr.push(Math.floor(Math.random() * 100000));
            if (i == 29) return arr;
        }
    }


    getDateRande(lenght: number): Date[] {
        const start = moment().subtract(lenght, 'd');
        const end = moment().subtract(1, 'd');
        const days = [];
        let day = start;

        while (day <= end) {
            days.push(day.toDate());
            day = day.clone().add(1, 'd');
        }

        return days;
    }

    setChart(): void {
        const ctx = this.canvas.nativeElement.getContext('2d');
        if (this.chart) this.chart.destroy();
        this.chart = new Chart(ctx, {
            type: 'line',
            data: {
                // labels: this.labels,
                datasets: this.data,
            },
            options: this.options,
        });
    }

    getUnit(length: number): string {
        if (length <= 31) return 'day';
        if (length <= 365) return 'month';
        return 'year';
    }
}
