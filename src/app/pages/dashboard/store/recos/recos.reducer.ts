import { RecosActions, RecosTypes } from './recos.action';
import { IRecos, IRecosBlock } from '../../../../models/interfaces/recos';

export class RecosState implements IRecos {
    stylePicker: IRecosBlock;
    internalPages: IRecosBlock;
    outOfStockPopup: IRecosBlock;
}

export function reducer(state = new RecosState(), action: RecosActions): RecosState {
    switch (action.type) {
        case RecosTypes.LoadDataSuccess:
            return action.payload;
        default: {
            return state;
        }
    }
}
