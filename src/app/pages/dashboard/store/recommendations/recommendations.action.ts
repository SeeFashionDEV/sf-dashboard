import { Action } from '@ngrx/store';
import { IRecommendation } from '../../../../models/interfaces/recomendation';

export enum RecommendationsTypes {
    LoadTopRecommendations = '[TOP-RECOMMENDATIONS] Load top recommendations [...]',
    LoadTopRecommendationsSuccess = '[TOP-RECOMMENDATIONS] Load top recommendations [SUCCESS]',
    LoadTopRecommendationsError = '[TOP-RECOMMENDATIONS] Load top recommendations [ERROR]',
    LoadMoreTopRecommendations = '[TOP-RECOMMENDATIONS] Load more top recommendations [...]',
    SetTopRecommendationsLength = '[TOP-RECOMMENDATIONS] Set top recommendations length [...]',
    OpenTopRecommendationDialog = '[TOP-RECOMMENDATIONS] Open top recommendation dialog [...]',

    LoadAllTopRecommendations = '[TOP-RECOMMENDATIONS] Load all top recommendations [...]',
    LoadAllTopRecommendationsSuccess = '[TOP-RECOMMENDATIONS] Load all top recommendations [SUCCESS]',
    LoadAllTopRecommendationsError = '[TOP-RECOMMENDATIONS] Load all top recommendations [ERROR]',
}

// LOAD RECOMMENDATIONS
export class LoadTopRecommendationsAction implements Action {
    readonly type = RecommendationsTypes.LoadTopRecommendations;
}

export class LoadMoreTopRecommendationsAction implements Action {
    readonly type = RecommendationsTypes.LoadMoreTopRecommendations;

    constructor(public payload: number) { }
}

export class LoadTopRecommendationsSuccessAction implements Action {
    readonly type = RecommendationsTypes.LoadTopRecommendationsSuccess;

    constructor(public payload: IRecommendation[]) { }
}

export class LoadTopRecommendationsErrorAction implements Action {
    readonly type = RecommendationsTypes.LoadTopRecommendationsError;

    constructor(public payload: any) { }
}

export class LoadAllTopRecommendationsAction implements Action {
    readonly type = RecommendationsTypes.LoadAllTopRecommendations;

    constructor(public payload: number) { }
}

export class LoadAllTopRecommendationsSuccessAction implements Action {
    readonly type = RecommendationsTypes.LoadAllTopRecommendationsSuccess;

    constructor(public payload: IRecommendation[]) { }
}

export class LoadAllTopRecommendationsErrorAction implements Action {
    readonly type = RecommendationsTypes.LoadAllTopRecommendationsError;

    constructor(public payload: any) { }
}

export class SetTopRecommendationsLengthAction implements Action {
    readonly type = RecommendationsTypes.SetTopRecommendationsLength;

    constructor(public payload: number) { }
}

export class OpenTopRecommendationDialogAction implements Action {
    readonly type = RecommendationsTypes.OpenTopRecommendationDialog;

    constructor(public payload: IRecommendation) { }
}

export type RecommendationsActions
    = LoadTopRecommendationsAction
    | LoadMoreTopRecommendationsAction
    | LoadTopRecommendationsSuccessAction
    | LoadTopRecommendationsErrorAction
    | SetTopRecommendationsLengthAction
    | OpenTopRecommendationDialogAction
    | LoadAllTopRecommendationsAction
    | LoadAllTopRecommendationsSuccessAction
    | LoadAllTopRecommendationsErrorAction
    ;