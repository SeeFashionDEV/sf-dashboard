import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getGeneralInfoState } from '../../../../store/index';
import { AppState } from '../../../../../../store';
import { IGeneralInfo } from '../../../../../../models/interfaces/general-info';

@Component({
    selector: 'db-general-info',
    templateUrl: './general-info.component.html',
    styleUrls: ['./general-info.component.scss'],
})
export class DashboardGeneralInfoComponent implements OnInit {
    data$: Observable<IGeneralInfo>;

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.data$ = this.store.select(getGeneralInfoState);
    }

}
