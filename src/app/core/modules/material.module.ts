import { NgModule } from '@angular/core';
import {
    MatInputModule, MatDialogModule, MatChipsModule, MatFormFieldModule,
    MatTooltipModule, MatSlideToggleModule, MatProgressSpinnerModule, MatButtonModule,
    MatToolbarModule, MatIconModule, MatSidenavModule, MatSnackBarModule, MatTableModule, MatPaginatorModule,
    MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatTabsModule, MatMenuModule,
    MatBadgeModule, MatSortModule, MatGridListModule, MatRadioModule, MatCheckboxModule,
    MatCardModule, MatProgressBarModule, MatListModule,
} from '@angular/material';

const MATERIAL_MODULES = [
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatDialogModule,
    MatFormFieldModule,
    MatChipsModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    MatBadgeModule,
    MatRadioModule,
    MatCheckboxModule,
    MatCardModule,
    MatGridListModule,
    MatListModule,
];

@NgModule({
    imports: [
        ...MATERIAL_MODULES,
    ],
    exports: [
        ...MATERIAL_MODULES,
    ],
})
export class MaterialModule { }
