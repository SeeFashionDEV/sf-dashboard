import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, empty } from 'rxjs';
import { AppState } from '../../../../store/index';
import { Store } from '@ngrx/store';
import { withLatestFrom, switchMap } from 'rxjs/operators';
import { FilterRange } from '../../../../models/enums/filter-range';
import { DataType } from '../../../../models/enums/data-type';
import { FilterActions, FilterTypes } from '../filter/filter.action';
import { IInfoBlockData } from '../../../../models/interfaces/info-block-data';
import { RecosActions, LoadRecosDataAction, RecosTypes, LoadRecosDataSuccessAction } from './recos.action';
import { IRecosBlock } from '../../../../models/interfaces/recos';

@Injectable()
export class RecosEffects {

    // @Effect()
    // loadData$: Observable<RecosActions> = this.actions$.pipe(
    //     ofType<LoadRecosDataAction>(RecosTypes.LoadData),
    //     withLatestFrom(this.store.select(getFilterState)),
    //     switchMap(([action, filter]) => {
    //         if (!filter) return empty();
    //         const stylePicker: IRecosBlock = this.getStylePicker(filter.range);
    //         const internalPages: IRecosBlock = this.getInternalPages(filter.range);
    //         const outOfStockPopup: IRecosBlock = this.getOutOfStockPopup(filter.range);
    //
    //         return of(new LoadRecosDataSuccessAction({
    //             stylePicker: stylePicker,
    //             internalPages: internalPages,
    //             outOfStockPopup: outOfStockPopup,
    //         }));
    //     }),
    // );

    @Effect()
    reloadData$: Observable<RecosActions> = this.actions$.pipe(
        ofType<FilterActions>(FilterTypes.ChangeFilterState),
        switchMap(() => of(new LoadRecosDataAction())),
    );

    constructor(
        private actions$: Actions,
        private store: Store<AppState>,
    ) { }

    getStylePicker(range: FilterRange): IRecosBlock {
        if (range == null) return;
        let renderedRecoBoxes: IInfoBlockData;
        let ctr: IInfoBlockData;
        let conversions: IInfoBlockData;
        let totalSales: IInfoBlockData;
        const v = {
            renderedRecoBoxes: {
                title: 'rendered recos boxes',
                dataType: DataType.NUMERIC,
            },
            ctr: {
                title: 'ctr',
                dataType: DataType.NUMERIC,
            },
            conversions: {
                title: 'conversions',
                dataType: DataType.NUMERIC,
            },
            totalSales: {
                title: 'total sales',
                dataType: DataType.NUMERIC,
            },
        };
        switch (range) {
            case FilterRange.MONTH:
                renderedRecoBoxes = {
                    value: 98355,
                    kpi: 11,
                };
                ctr = {
                    value: 11.4,
                    kpi: 9,
                    // bar2: {
                    //     seeFashion: 1600,
                    //     standard: 120,
                    // },
                };
                conversions = {
                    value: 6,
                    kpi: 0,
                    // bar2: {
                    //     seeFashion: 1200,
                    //     standard: 160,
                    // },
                };
                totalSales = {
                    value: 14501.9,
                    kpi: 5,
                    // bar2: {
                    //     seeFashion: 1000,
                    //     standard: 960,
                    // },
                };
                break;
            case FilterRange.WEEK:
                renderedRecoBoxes = {
                    value: 19137,
                    kpi: 11,
                };
                ctr = {
                    value: 11.4,
                    kpi: 9,
                    // bar2: {
                    //     seeFashion: 600,
                    //     standard: 500,
                    // },
                };
                conversions = {
                    value: 6,
                    kpi: 0,
                    // bar2: {
                    //     seeFashion: 200,
                    //     standard: 700,
                    // },
                };
                totalSales = {
                    value: 3742.4,
                    kpi: 5,
                    // bar2: {
                    //     seeFashion: 400,
                    //     standard: 400,
                    // },
                };
                break;
            case FilterRange.TODAY:
                renderedRecoBoxes = {
                    value: 2502,
                    kpi: 11,
                };
                ctr = {
                    value: 11.4,
                    kpi: 9,
                    // bar2: {
                    //     seeFashion: 120,
                    //     standard: 120,
                    // },
                };
                conversions = {
                    value: 6,
                    kpi: 0,
                    // bar2: {
                    //     seeFashion: 820,
                    //     standard: 350,
                    // },
                };
                totalSales = {
                    value: 351.8,
                    kpi: 5,
                    // bar2: {
                    //     seeFashion: 600,
                    //     standard: 120,
                    // },
                };
                break;
        }
        return {
            renderedRecoBoxes: {
                ...v.renderedRecoBoxes,
                ...renderedRecoBoxes,
            },
            ctr: {
                ...v.ctr,
                ...ctr,
            },
            conversions: {
                ...v.conversions,
                ...conversions,
            },
            totalSales: {
                ...v.totalSales,
                ...totalSales,
            },
        };
    }

    getInternalPages(range: FilterRange): IRecosBlock {
        if (range == null) return;
        let renderedRecoBoxes: IInfoBlockData;
        let ctr: IInfoBlockData;
        let conversions: IInfoBlockData;
        let totalSales: IInfoBlockData;
        const v = {
            renderedRecoBoxes: {
                title: 'rendered recos boxes',
                dataType: DataType.NUMERIC,
            },
            ctr: {
                title: 'ctr',
                dataType: DataType.NUMERIC,
            },
            conversions: {
                title: 'conversions',
                dataType: DataType.NUMERIC,
            },
            totalSales: {
                title: 'total sales',
                dataType: DataType.NUMERIC,
            },
        };
        switch (range) {
            case FilterRange.MONTH:
                renderedRecoBoxes = {
                    value: 2502,
                    kpi: -3,
                };
                ctr = {
                    value: 11.40,
                    kpi: 2,
                    // bar2: {
                    //     seeFashion: 1600,
                    //     standard: 120,
                    // },
                };
                conversions = {
                    value: 6,
                    kpi: 5,
                    // bar2: {
                    //     seeFashion: 1200,
                    //     standard: 160,
                    // },
                };
                totalSales = {
                    value: 2501.72916,
                    kpi: -3,
                    // bar2: {
                    //     seeFashion: 1000,
                    //     standard: 960,
                    // },
                };
                break;
            case FilterRange.WEEK:
                renderedRecoBoxes = {
                    value: 2502,
                    kpi: -3,
                };
                ctr = {
                    value: 11.40,
                    kpi: 2,
                    // bar2: {
                    //     seeFashion: 600,
                    //     standard: 500,
                    // },
                };
                conversions = {
                    value: 6,
                    kpi: 5,
                    // bar2: {
                    //     seeFashion: 200,
                    //     standard: 700,
                    // },
                };
                totalSales = {
                    value: 2501.72916,
                    kpi: -3,
                    // bar2: {
                    //     seeFashion: 400,
                    //     standard: 400,
                    // },
                };
                break;
            case FilterRange.TODAY:
                renderedRecoBoxes = {
                    value: 2502,
                    kpi: -3,
                };
                ctr = {
                    value: 11.40,
                    kpi: 2,
                    // bar2: {
                    //     seeFashion: 120,
                    //     standard: 120,
                    // },
                };
                conversions = {
                    value: 6,
                    kpi: 5,
                    // bar2: {
                    //     seeFashion: 820,
                    //     standard: 350,
                    // },
                };
                totalSales = {
                    value: 2501.72916,
                    kpi: -3,
                    // bar2: {
                    //     seeFashion: 600,
                    //     standard: 120,
                    // },
                };
                break;
        }
        return {
            renderedRecoBoxes: {
                ...v.renderedRecoBoxes,
                ...renderedRecoBoxes,
            },
            ctr: {
                ...v.ctr,
                ...ctr,
            },
            conversions: {
                ...v.conversions,
                ...conversions,
            },
            totalSales: {
                ...v.totalSales,
                ...totalSales,
            },
        };
    }

    getOutOfStockPopup(range: FilterRange): IRecosBlock {
        if (range == null) return;
        let renderedRecoBoxes: IInfoBlockData;
        let ctr: IInfoBlockData;
        let conversions: IInfoBlockData;
        let totalSales: IInfoBlockData;
        const v = {
            renderedRecoBoxes: {
                title: 'rendered recos boxes',
                dataType: DataType.NUMERIC,
            },
            ctr: {
                title: 'ctr',
                dataType: DataType.NUMERIC,
            },
            conversions: {
                title: 'conversions',
                dataType: DataType.NUMERIC,
            },
            totalSales: {
                title: 'total sales',
                dataType: DataType.NUMERIC,
            },
        };
        switch (range) {
            case FilterRange.MONTH:
                renderedRecoBoxes = {
                    value: 11,
                    kpi: 2,
                };
                ctr = {
                    value: 6.00,
                    kpi: 4,
                    // bar2: {
                    //     seeFashion: 1600,
                    //     standard: 120,
                    // },
                };
                conversions = {
                    value: 352,
                    kpi: 2,
                    // bar2: {
                    //     seeFashion: 1200,
                    //     standard: 160,
                    // },
                };
                totalSales = {
                    value: 0,
                    // kpi: 5,
                    // bar2: {
                    //     seeFashion: 1000,
                    //     standard: 960,
                    // },
                };
                break;
            case FilterRange.WEEK:
                renderedRecoBoxes = {
                    value: 11,
                    kpi: 2,
                };
                ctr = {
                    value: 6,
                    kpi: 4,
                    // bar2: {
                    //     seeFashion: 600,
                    //     standard: 500,
                    // },
                };
                conversions = {
                    value: 352,
                    kpi: 2,
                    // bar2: {
                    //     seeFashion: 200,
                    //     standard: 700,
                    // },
                };
                totalSales = {
                    value: 0,
                    // kpi: 5,
                    // bar2: {
                    //     seeFashion: 400,
                    //     standard: 400,
                    // },
                };
                break;
            case FilterRange.TODAY:
                renderedRecoBoxes = {
                    value: 11,
                    kpi: 2,
                };
                ctr = {
                    value: 6,
                    kpi: 4,
                    // bar2: {
                    //     seeFashion: 120,
                    //     standard: 120,
                    // },
                };
                conversions = {
                    value: 352,
                    kpi: 2,
                    // bar2: {
                    //     seeFashion: 820,
                    //     standard: 350,
                    // },
                };
                totalSales = {
                    value: 0,
                    // kpi: 5,
                    // bar2: {
                    //     seeFashion: 600,
                    //     standard: 120,
                    // },
                };
                break;
        }
        return {
            renderedRecoBoxes: {
                ...v.renderedRecoBoxes,
                ...renderedRecoBoxes,
            },
            ctr: {
                ...v.ctr,
                ...ctr,
            },
            conversions: {
                ...v.conversions,
                ...conversions,
            },
            totalSales: {
                ...v.totalSales,
                ...totalSales,
            },
        };
    }
}
