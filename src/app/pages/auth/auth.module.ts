import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        CoreModule,
        ReactiveFormsModule,
    ],
    exports: [],
    declarations: [
        AuthComponent,
    ],
    providers: [

    ],
})
export class AuthModule { }
