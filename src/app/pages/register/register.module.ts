import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        CoreModule,
        ReactiveFormsModule,
    ],
    exports: [],
    declarations: [
        RegisterComponent,
    ],
    providers: [

    ],
})
export class RegisterModule { }
