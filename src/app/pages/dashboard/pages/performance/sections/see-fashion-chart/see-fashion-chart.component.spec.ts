import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSeeFashionChartComponent } from './see-fashion-chart.component';

describe('SeeFashionChartComponent', () => {
  let component: DashboardSeeFashionChartComponent;
  let fixture: ComponentFixture<DashboardSeeFashionChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardSeeFashionChartComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSeeFashionChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
