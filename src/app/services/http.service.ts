import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of, empty } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { IRecommendation } from '../models/interfaces/recomendation';
import { Store } from '@ngrx/store';
import { AppState } from '../store';
import { SetTopRecommendationsLengthAction } from '../pages/dashboard/store/recommendations/recommendations.action';

@Injectable()
export class HttpService {

    constructor(
        private config: ConfigService,
        private http: HttpClient,
        private store: Store<AppState>,
    ) {
    }

    get apiUrl(): string {
        return this.config.apiUrl;
    }

    loadData(prop: { startDate: string, endDate: string }): Observable<any> {
        return this.http.get<any>(`${this.apiUrl}/dashboard?startDate=${prop.startDate}&endDate=${prop.endDate}`);
    }

    // loadData(prop: { startDate: string, endDate: string }): Observable<any> {
    //     return this.http.get<any>(`https://cloud.seefashion-insights.com/api/admin/www.mock-fashion-shop.com` +
    //         `/dashboard?startDate=${prop.startDate}&endDate=${prop.endDate}`);
    // }

    loadProductRevenue(page: number, size: number, shopId: number): Observable<any> {
        return this.http.get<any>(`${this.apiUrl}/rec-product-revenue?page=${page}&size=${size}&shopId=${shopId}`).pipe(
            map(data => {
                return data;
            }),
        );
    }

    loadTopRecommendations(shopId: number, size: number = 8): Observable<IRecommendation[]> {
        return this.http.get<any>(`${this.apiUrl}/performance/most-click-product?size=${size}&shopId=${shopId}`).pipe(
            tap((res) => this.store.dispatch(new SetTopRecommendationsLengthAction(res.totalElements))),
            map(res => {
                return res.content;
            }),
        );
    }

}
