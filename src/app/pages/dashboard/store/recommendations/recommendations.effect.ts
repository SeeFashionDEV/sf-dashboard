import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { tap, map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { Observable, of, empty } from 'rxjs';
import {
    RecommendationsActions, LoadTopRecommendationsAction,
    RecommendationsTypes, LoadTopRecommendationsSuccessAction,
    LoadTopRecommendationsErrorAction,
    LoadMoreTopRecommendationsAction,
    OpenTopRecommendationDialogAction,
    LoadAllTopRecommendationsAction,
    LoadAllTopRecommendationsSuccessAction,
    LoadAllTopRecommendationsErrorAction,
} from './recommendations.action';
import { MatDialog, MatDialogRef } from '@angular/material';
import {
    TopRecommendationsDialogComponent,
} from '../../core/modals/top-recommendations-dialog/top-recommendations-dialog.component';
import { HttpService } from '../../../../services/http.service';
import { AppState } from '../../../../store';

@Injectable()
export class Recommendationsffects {
    recommendationsDialogRef: MatDialogRef<TopRecommendationsDialogComponent>;

    @Effect()
    loadRecommendations$: Observable<RecommendationsActions> = this.actions$.pipe(
        ofType<LoadTopRecommendationsAction>(RecommendationsTypes.LoadTopRecommendations),
        withLatestFrom(this.store.select(s => s.auth)),
        switchMap(prop => {
            return this.httpService.loadTopRecommendations(prop[1].accountInfo.shopifyId).pipe(
                map(res => new LoadTopRecommendationsSuccessAction(res)),
                catchError(error => of(new LoadTopRecommendationsErrorAction(error))),
            );
        }),
    );

    @Effect()
    loadAllRecommendations$: Observable<RecommendationsActions> = this.actions$.pipe(
        ofType<LoadAllTopRecommendationsAction>(RecommendationsTypes.LoadAllTopRecommendations),
        map((action: LoadAllTopRecommendationsAction) => action.payload),
        withLatestFrom(this.store.select(s => s.auth)),
        switchMap(([size, prop]) => {
            if (!prop) {
                return empty();
            } else {
                return of({ size: size, id: prop.accountInfo.shopifyId });
            }

        }),
        switchMap(obj => {
            return this.httpService.loadTopRecommendations(obj.id, obj.size).pipe(
                map(res => new LoadAllTopRecommendationsSuccessAction(res)),
                catchError(error => of(new LoadAllTopRecommendationsErrorAction(error))),
            );
        }),
    );

    @Effect()
    loadMoreReccomendations$: Observable<RecommendationsActions> = this.actions$.pipe(
        ofType<LoadMoreTopRecommendationsAction>(RecommendationsTypes.LoadMoreTopRecommendations),
        map((action: LoadMoreTopRecommendationsAction) => action.payload),
        withLatestFrom(this.store.select(s => s.auth)),
        switchMap(([size, prop]) => {
            if (!prop) {
                return empty();
            } else {
                return of({ size: size, id: prop.accountInfo.shopifyId });
            }

        }),
        switchMap(obj => {
            return this.httpService.loadTopRecommendations(obj.id, obj.size).pipe(
                map(res => new LoadTopRecommendationsSuccessAction(res)),
                catchError(error => of(new LoadTopRecommendationsErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<RecommendationsActions> = this.actions$.pipe(
        ofType<LoadTopRecommendationsErrorAction | LoadAllTopRecommendationsErrorAction>(
            RecommendationsTypes.LoadTopRecommendationsError,
            RecommendationsTypes.LoadAllTopRecommendationsError,
        ),
        map(action => action.payload),
        tap(e => {
            console.error(e);
        }),
    );

    @Effect()
    recommendationDialog$: Observable<RecommendationsActions> = this.actions$.pipe(
        ofType<OpenTopRecommendationDialogAction>(RecommendationsTypes.OpenTopRecommendationDialog),
        map(action => action.payload),
        switchMap(card => {
            this.recommendationsDialogRef = this.dialog.open(TopRecommendationsDialogComponent);
            this.recommendationsDialogRef.componentInstance.card = card;
            return empty();
        }),
    );

    constructor(
        private actions$: Actions,
        private store: Store<AppState>,
        private httpService: HttpService,
        private dialog: MatDialog,
    ) { }

}