import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { tap, map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { Observable, empty, of } from 'rxjs';
import {
    AuthActions, LoginAction, AuthTypes, LoginErrorAction, LoginSuccessAction,
    LogoutAction,
    SetAccountInfoSuccessAction,
    SetAccountInfoErrorAction,
    OpenProfileDialogsAction,
} from './auth.action';
import { AuthService } from '../../services/auth.service';
import { LocalStorageService } from '../../services/local-storage.service';
import { StorageItem } from '../../models/enums/storage-item';
import { Router } from '@angular/router';
import { IAccountInfo } from '../../models/interfaces/account-info';
import { SetAccountInfoAction } from './auth.action';
import { MatDialog } from '@angular/material';
import {
    MyProfileDialogComponent,
} from '../../pages/dashboard/core/modals/my-profile-dialog/my-profile-dialog.component';

@Injectable()
export class AuthEffects {
    @Effect()
    login$: Observable<AuthActions> = this.actions$.pipe(
        ofType<LoginAction>(AuthTypes.Login),
        map(action => action.payload),
        switchMap(creds =>
            this.authService.login(creds).pipe(
                map(res => new LoginSuccessAction({token: res.headers.get('authorization'), accountInfo: res.body})),
                catchError(error => of(new LoginErrorAction(error))),
            ),
        ),
    );

    @Effect({dispatch: false})
    onLoginSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<LoginSuccessAction>(AuthTypes.LoginSuccess),
        map(action => action.payload),
        tap((res: { token: string, accountInfo: IAccountInfo }) => {
            const token = res.token;
            const accountInfo = res.accountInfo;
            if (!token || !accountInfo) return;
            this.localStorage.setItem(StorageItem.Token, token.substring(7));
            this.localStorage.setItem(StorageItem.AccountInfo, JSON.stringify(accountInfo));
            this.router.navigate(['/']);
        }),
        switchMap(() => empty()),
    );

    @Effect()
    setAccountInfo$: Observable<Action> = this.actions$.pipe(
        ofType<SetAccountInfoAction>(AuthTypes.SetAccountInfo),
        map(() => {
            const info: string = this.localStorage.getItem(StorageItem.AccountInfo);
            if (!info) throw new Error('No account info found!');
            const accountInfo: IAccountInfo = <IAccountInfo>JSON.parse(info);
            return new SetAccountInfoSuccessAction(accountInfo);
        }),
        catchError(e => of(new SetAccountInfoErrorAction(e))),
    );

    @Effect({dispatch: false})
    onLogout$: Observable<Action> = this.actions$.pipe(
        ofType<LogoutAction>(AuthTypes.Logout),
        tap(() => {
            this.localStorage.removeItem(StorageItem.Token);
            this.localStorage.removeItem(StorageItem.AccountInfo);
            this.router.navigate(['/login']);
        }),
    );

    @Effect({dispatch: false})
    onError$: Observable<Action> = this.actions$.pipe(
        ofType<LoginErrorAction | SetAccountInfoErrorAction>
        (AuthTypes.LoginError, AuthTypes.SetAccountInfoError),
        map(action => action.payload),
        tap(e => {
            console.error(e);
        }),
    );

    @Effect()
    openProfileDialog$: Observable<AuthActions> = this.actions$.pipe(
        ofType<OpenProfileDialogsAction>(AuthTypes.OpenProfileDialog),
        switchMap(() => {
            this.dialog.open(MyProfileDialogComponent);
            return empty();
        }),
    );

    constructor(
        private actions$: Actions,
        private authService: AuthService,
        private localStorage: LocalStorageService,
        private router: Router,
        private dialog: MatDialog,
    ) {
    }

}
