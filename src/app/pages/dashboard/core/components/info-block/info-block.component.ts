import { Component, OnInit, Input } from '@angular/core';
import { DataType } from '../../../../../models/enums/data-type';
import { IInfoBlockData } from '../../../../../models/interfaces/info-block-data';

@Component({
    selector: 'app-info-block',
    templateUrl: './info-block.component.html',
    styleUrls: ['./info-block.component.scss'],
})
export class DashboardInfoBlockComponent {
    DataType = DataType;
    @Input() title: string;
    @Input() data: IInfoBlockData;
    // @Input() value: number;
    // @Input() dataType: DataType = DataType.NUMERIC;
    // @Input() p1: number;
    // @Input() bar1: { v1: number, v2: number, p1?: number, p2?: number };
    // @Input() bar2: { v1: number, v2: number };
    // @Input() isCurrency: boolean = false;
    // @Input() isBarCurrecncy: boolean = false;


}
