import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardInfoBlockComponent } from './info-block.component';

describe('InfoBlockComponent', () => {
  let component: DashboardInfoBlockComponent;
  let fixture: ComponentFixture<DashboardInfoBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardInfoBlockComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardInfoBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
