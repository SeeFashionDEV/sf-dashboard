import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/index';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InstallationAction } from '../../store/installation/installation.action';


@Component({
    selector: 'app-installation',
    templateUrl: './installation.component.html',
    styleUrls: ['./installation.component.scss'],
})
export class InstallationComponent implements OnInit {
    form: FormGroup;

    constructor(
        private store: Store<AppState>,
        private fb: FormBuilder,
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            shopDomain: ['', [Validators.required]],
        });
    }

    onInstall(): void {
        if (this.form.invalid) return;
        const data = this.form.value;
        this.store.dispatch(new InstallationAction(data));
    }
}
