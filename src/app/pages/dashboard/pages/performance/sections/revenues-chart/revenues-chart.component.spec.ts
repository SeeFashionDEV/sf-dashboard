import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRevenuesChartComponent } from './revenues-chart.component';

describe('RevenuesChartComponent', () => {
    let component: DashboardRevenuesChartComponent;
    let fixture: ComponentFixture<DashboardRevenuesChartComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardRevenuesChartComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardRevenuesChartComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
