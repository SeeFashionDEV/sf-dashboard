import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardBar1Component } from './bar1.component';

describe('Bar1Component', () => {
    let component: DashboardBar1Component;
    let fixture: ComponentFixture<DashboardBar1Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardBar1Component],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardBar1Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
