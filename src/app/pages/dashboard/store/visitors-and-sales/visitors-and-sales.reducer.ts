import { VisitorsAndSalesTypes, VisitorsAndSalesActions } from './visitors-and-sales.action';
import { IVisitorsAndSales } from '../../../../models/interfaces/visitors-and-sales';

export class VisitorsAndSalesState implements IVisitorsAndSales {
    visitors: any;
    addToCard: any;
    sales: any;
}

export function reducer(state = new VisitorsAndSalesState(), action: VisitorsAndSalesActions): VisitorsAndSalesState {
    switch (action.type) {
        case VisitorsAndSalesTypes.LoadData:
            return action.payload;
        default: {
            return state;
        }
    }
}
