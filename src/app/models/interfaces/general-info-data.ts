import { IData } from './data';
import { DataType } from '../enums/data-type';

export interface IGeneralInfoData {
    title: string;
    helpMessage: string;
    total: IData;
    standard: IData;
    seeFashion: IData;
    dataType: DataType;
}