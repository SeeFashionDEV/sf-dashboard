import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injector } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import 'rxjs/add/operator/catch';
import { LocalStorageService } from './local-storage.service';
import { StorageItem } from '../models/enums/storage-item';

@Injectable()
export class AccessInterceptor implements HttpInterceptor {
    constructor(
        private injector: Injector,
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const ls = this.injector.get(LocalStorageService);
        const token = ls.getItem(StorageItem.Token);
        if (token) {
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`,
                },
            });
        }
        return next.handle(req).catch(err => {
            const status = err.status;

            return throwError(err);
        });

    }

}