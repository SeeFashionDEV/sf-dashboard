import { DashboardGeneralInfoComponent } from './general-info/general-info.component';
import { DashboardRevenuesChartComponent } from './revenues-chart/revenues-chart.component';
import { DashboardVisitorsAndSalesComponent } from './visitors-and-sales/visitors-and-sales.component';
import { DashboardRecosComponent } from './recos/recos.component';
import { DashboardTopRecomendationsComponent } from './top-recomendations/top-recomendations.component';
import { DashboardOpportunitiesComponent } from './opportunities/opportunities.component';
import { DashboardFilterComponent } from './filter/filter.component';
import { DashboardSeeFashionChartComponent } from './see-fashion-chart/see-fashion-chart.component';

export const DASHBOARD_SECTIONS = [
    DashboardGeneralInfoComponent,
    DashboardRevenuesChartComponent,
    DashboardVisitorsAndSalesComponent,
    DashboardRecosComponent,
    DashboardTopRecomendationsComponent,
    DashboardOpportunitiesComponent,
    DashboardFilterComponent,
    DashboardSeeFashionChartComponent,
];
