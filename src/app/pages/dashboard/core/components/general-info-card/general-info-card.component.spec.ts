import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardGeneralInfoCardComponent } from './general-info-card.component';

describe('GeneralInfoCardComponent', () => {
    let component: DashboardGeneralInfoCardComponent;
    let fixture: ComponentFixture<DashboardGeneralInfoCardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardGeneralInfoCardComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardGeneralInfoCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
