import { Action } from '@ngrx/store';
import { IRegister } from '../../models/interfaces/IRegister';
import { IAccountInfo } from '../../models/interfaces/account-info';

export enum RegisterTypes {
    OpenRegister = '[REG] Open Register [...]',
    Register = '[REG] Register [SUCCESS]',
    RegisterSuccess = '[REG] Register [SUCCESS]',
    RegisterError = '[REG] Register [ERROR]',
    SetAccountInfo = '[REG] Register and Login [SET]',
    SetAccountInfoSuccess = '[REG] Register and Login [SUCCESS]',
    SetAccountInfoError = '[REG] Register and Login [ERROR]',
}

export class RegisterAction implements Action {
    readonly type = RegisterTypes.Register;

    constructor(public payload: IRegister) { }
}

export class OpenRegisterAction implements Action {
    readonly type = RegisterTypes.OpenRegister;
}


export class RegisterSuccessAction implements Action {
    readonly type = RegisterTypes.RegisterSuccess;

    constructor(public payload: { token: string, accountInfo: any }) { }
}

export class RegisterErrorAction implements Action {
    readonly type = RegisterTypes.RegisterError;
    constructor(public payload: any) { }
}

export class SetAccountInfoAction implements Action {
    readonly type = RegisterTypes.SetAccountInfo;
}

export class SetAccountInfoSuccessAction implements Action {
    readonly type = RegisterTypes.SetAccountInfoSuccess;

    constructor(public payload: IAccountInfo) { }
}

export class SetAccountInfoErrorAction implements Action {
    readonly type = RegisterTypes.SetAccountInfoError;

    constructor(public payload: any) { }
}

export type RegActions
    = RegisterAction
    | OpenRegisterAction
    | RegisterErrorAction
    | RegisterSuccessAction
    | SetAccountInfoAction
    | SetAccountInfoSuccessAction
    | SetAccountInfoErrorAction
    ;
