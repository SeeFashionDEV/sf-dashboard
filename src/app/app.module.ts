import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_ROUTES } from './app.routing';
import { RouterModule, Routes } from '@angular/router';
import { APP_GUARDS } from './guards';
import { AuthModule } from './pages/auth/auth.module';
import { RegisterModule } from './pages/register/register.module';
import { APP_SERVICES } from './services';
import { ConfigService } from './services/config.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { appEffects, appReducers } from './store';
import { metaReducers } from './store/index';
import { AccessInterceptor } from './services/access-interceptor.service';
import { DashboarModalsModule } from './pages/dashboard/core/modals/dashboard-modals.module.ts.module';
import { InstallationModule } from './pages/installation/installation.module';


export function init(config: ConfigService): any {
    return () => config.load();
}

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(APP_ROUTES),
        AuthModule,
        RegisterModule,
        InstallationModule,
        DashboarModalsModule,
        StoreModule.forRoot(appReducers, { metaReducers }),
        !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 50 }) : [],
        EffectsModule.forRoot(appEffects),
    ],
    declarations: [
        AppComponent,
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: init,
            deps: [ConfigService],
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AccessInterceptor,
            multi: true,
        },
        ...APP_GUARDS,
        ...APP_SERVICES,
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }
