import { Component, OnInit, Input, ElementRef, HostListener } from '@angular/core';

@Component({
    selector: 'app-dropdown-menu',
    templateUrl: './dropdown-menu.component.html',
    styleUrls: ['./dropdown-menu.component.scss'],
})

export class DropdownMenuComponent implements OnInit {

    @Input() label: string;
    isActive: boolean = false;

    constructor(private _eref: ElementRef) { }

    ngOnInit(): void {

    }

    toggleState(): void {
        this.isActive = !this.isActive;
    }

    @HostListener('document:click', ['$event'])
    onClick(event): void {
        if (!this._eref.nativeElement.contains(event.target))
            this.isActive = false;
    }

}
