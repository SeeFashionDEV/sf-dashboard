import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as filter from './filter/filter.reducer';
import * as recommendations from './recommendations/recommendations.reducer';
import * as generalInfo from './general-info/general-info.reducer';
import * as opportunities from './opportunities/opportunities.reducer';
import * as visitorsAndSales from './visitors-and-sales/visitors-and-sales.reducer';
import * as recos from './recos/recos.reducer';

export interface DashboardState {
    filter: filter.FilterState;
    recommendations: recommendations.TopRecommendationsState;
    generalInfo: generalInfo.GeneralInfoState;
    visitorsAndSales: visitorsAndSales.VisitorsAndSalesState;
    opportunities: opportunities.OpportunitiesState;
    recos: recos.RecosState;
}

export const dbReducers: ActionReducerMap<DashboardState> = {
    filter: filter.reducer,
    recommendations: recommendations.reducer,
    generalInfo: generalInfo.reducer,
    visitorsAndSales: visitorsAndSales.reducer,
    opportunities: opportunities.reducer,
    recos: recos.reducer,
};

export const getDashboardState = createFeatureSelector<DashboardState>('dashboard');

// export const getFilterState = createSelector(
//     getDashboardState,
//     (state: DashboardState) => state.filter.filterState,
// );

export const topRecommendationsState = createSelector(
    getDashboardState,
    (state: DashboardState) => state.recommendations,
);

export const getGeneralInfoState = createSelector(
    getDashboardState,
    (state: DashboardState) => state.generalInfo,
);

export const getVisitorsAndSalesState = createSelector(
    getDashboardState,
    (state: DashboardState) => state.visitorsAndSales,
);

export const getOpportunitiesState = createSelector(
    getDashboardState,
    (state: DashboardState) => state.opportunities,
);

export const getRecosState = createSelector(
    getDashboardState,
    (state: DashboardState) => state.recos,
);

export const getActiveRegion = createSelector(
    getDashboardState,
    (state: DashboardState) => state.opportunities.activeRegion,
);
