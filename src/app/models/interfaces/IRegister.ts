export interface IRegister {
    firstName: string;
    lastName: string;
    email: string;
    shopDomain: string;
    password: string;
    password2: string;
}
