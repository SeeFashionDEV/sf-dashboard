import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'db-value-dynamic',
  templateUrl: './value-dynamic.component.html',
  styleUrls: ['./value-dynamic.component.scss'],
})
export class ValueDynamicComponent implements OnInit {
  @Input() value: number;
  @Input() color: string = 'green';

  constructor() { }

  get roundedValue(): number {
    if (!this.value) return;
    return Math.abs(this.value);
  }
  ngOnInit(): void {
  }

}
