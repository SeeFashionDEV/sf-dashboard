import { FilterActions, FilterTypes } from './filter.action';
import { IFilterState } from '../../../../models/interfaces/filter-state';
import { FilterRange } from '../../../../models/enums/filter-range';

export class FilterState {
    // filterState: IFilterState;
    // constructor() {
    //     // this.filterState = {
    //     //     range: FilterRange.MONTH,
    //     // };
    // }
}

export function reducer(state = new FilterState(), action: FilterActions): FilterState {
    switch (action.type) {
        // case FilterTypes.ChangeFilterState:
        //     return {
        //         ...state,
        //         filterState: action.payload,
        //     };
        default: {
            return state;
        }
    }
}
