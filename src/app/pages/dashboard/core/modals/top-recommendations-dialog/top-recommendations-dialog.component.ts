import { Component, OnInit } from '@angular/core';
import { IRecommendation } from '../../../../../models/interfaces/recomendation';

@Component({
  selector: 'db-top-recommendations-dialog',
  templateUrl: './top-recommendations-dialog.component.html',
  styleUrls: ['./top-recommendations-dialog.component.scss'],
})
export class TopRecommendationsDialogComponent implements OnInit {
  card: IRecommendation;

  constructor() { }

  ngOnInit(): void {
  }

}
