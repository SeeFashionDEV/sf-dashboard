import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAppConfig } from '../models/interfaces/config';

@Injectable()
export class ConfigService {
    private config: IAppConfig;
    private configUrl: string = '/config.json';

    constructor(
        private http: HttpClient,
        @Inject(PLATFORM_ID) private platformId: string,
    ) { }

    get apiUrl(): string {
        return this.config.apiUrl;
    }

    get serverUrl(): string {
        return this.config.serverUrl;
    }

    load(): Promise<any> {
        const promise: Promise<Object> = this.http.get(this.configUrl).toPromise();
        promise.then((config: IAppConfig) => {
            this.config = config;
        });
        return promise;
    }

}
