import { IInfoBlockData } from './info-block-data';

export interface IVisitorsData {
    uniqueVisitors: IInfoBlockData;
    numberOfProductsViewed: IInfoBlockData;
    averageTimePerProductView: IInfoBlockData;
    numberOfRecomendations: IInfoBlockData;
}

export interface IAddToCartData {
    totalAddToCarts: IInfoBlockData;
    totalValueOfAddToCarts: IInfoBlockData;
    averageNumberOfItemsPerVisitor: IInfoBlockData;
}

export interface ISalesData {
    revenues: IInfoBlockData;
    revenuePerVisit: IInfoBlockData;
    revenuePerProductView: IInfoBlockData;
    averageOrderSize: IInfoBlockData;
}

export interface IVisitorsAndSales {
    visitors: IVisitorsData;
    addToCard: IAddToCartData;
    sales: ISalesData;
}

