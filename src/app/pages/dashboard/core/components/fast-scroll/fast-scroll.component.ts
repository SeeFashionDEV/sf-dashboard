import { Component, Input } from '@angular/core';
import { ScrollToPosition, ScrollToBehavior, ScrollService } from '../../../../../services/scroll.service';
import { fadeInOut } from '../../../../../core/animations/fade.animation';

@Component({
    selector: 'db-fast-scroll',
    templateUrl: 'fast-scroll.component.html',
    styleUrls: ['fast-scroll.component.scss'],
    animations: [fadeInOut],
})
export class FastScrollComponent {
    @Input() direction: ScrollToPosition = ScrollToPosition.Start;
    @Input() behavior: ScrollToBehavior = ScrollToBehavior.Smooth;

    constructor(
        private scrollService: ScrollService,
    ) { }

    scroll(): void {
        this.scrollService.scrollTo(this.direction, this.behavior);
    }
}