import { Component, OnInit, Input } from '@angular/core';
import { DataType } from '../../../../../models/enums/data-type';

@Component({
    selector: 'db-bar2',
    templateUrl: './bar2.component.html',
    styleUrls: ['./bar2.component.scss'],
})
export class DashboardBar2Component {
    DataType = DataType;
    @Input() v1: number;
    @Input() v2: number;
    @Input() dataType: DataType = DataType.NUMERIC;
}
