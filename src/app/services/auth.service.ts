import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICredentials } from '../models/interfaces/credentials';
import { IAccountInfo } from '../models/interfaces/account-info';

@Injectable()
export class AuthService {

    constructor(
        private config: ConfigService,
        private http: HttpClient,
    ) { }

    get apiUrl(): string {
        return this.config.apiUrl;
    }

    login(creds: ICredentials): Observable<HttpResponse<IAccountInfo>> {
        return this.http.post<IAccountInfo>(`${this.apiUrl}/login`, creds, { observe: 'response' });
    }

}
