import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { FilterRange } from '../../../../../../models/enums/filter-range';
declare var Chart: any;

@Component({
    selector: 'db-linear-chart',
    templateUrl: './linear-chart.component.html',
    styleUrls: ['./linear-chart.component.scss'],
})
export class DashboardLinearChartComponent implements OnInit {
    label: string;
    tot: number;
    @Input() range: FilterRange = FilterRange.WEEK;
    @Input() set data(value: any) {
        this.chartData[0].data = value.data;
        this.label = value.label;
        this.tot = value.tot;
    }
    @ViewChild('canvas') canvas: ElementRef;
    chart: any;
    chartData = [
        {
            data: this.data,
            fill: false,
            borderColor: [
                '#FFF',
            ],
            borderWidth: 1,
        },
    ];

    options = {
        elements: {
            line: {
                tension: 0,
            },
        },
        legend: {
            display: false,
        },
        maintainAspectRatio: true,
        scales: {
            yAxes: [{
                display: false,
                ticks: {
                    beginAtZero: true,
                    fontColor: '#FFF',
                },
            }],
            xAxes: [{
                display: false,
                ticks: {
                    fontColor: '#FFF',
                },
                position: 'top',
                type: 'time',
                // time for week
                time: {
                    tooltipFormat: 'MMM Do YY',
                    unit: 'day',
                    displayFormats: {
                        day: 'dddd',
                    },
                },
                // time for month
                // time: {
                //     unit: 'day',
                //     displayFormats: {
                //         millisecond: 'DD',
                //         day: 'DD',
                //     },
                // },
            }],
        },
    };

    ngOnInit(): void {
        this.setChart();
    }

    // ngOnChanges(changes: SimpleChanges): void {
    //     if (changes && changes.range && changes.range.currentValue !== undefined) {
    //         let axisType;
    //         let labels;
    //         const range = changes.range.currentValue;
    //         if (!range) {
    //             axisType = 'day';
    //         }
    //         switch (range) {
    //             case ChartRange.WEEK:
    //                 axisType = 'day';
    //                 labels = this.getDateRande(7);
    //                 this.options.scales.xAxes[0]['labels'] = labels;
    //                 break;
    //             case ChartRange.MONTH:
    //                 axisType = 'day';
    //                 labels = this.getDateRande(30);
    //                 this.options.scales.xAxes[0]['labels'] = labels;
    //                 // this.options.scales.xAxes[0].time.['displayFormats'] = {
    //                 //     millisecond: 'DD',
    //                 //     day: 'DD',
    //                 // };
    //                 break;
    //             case ChartRange.TODAY:
    //                 axisType = 'hour';
    //                 break;
    //             case ChartRange.RANGE:
    //                 // if (range <= 14) {
    //                 //     axisType = 'day';
    //                 // } else {
    //                 //     axisType = 'month';
    //                 // }
    //                 break;
    //             default:
    //                 break;
    //         }
    //         this.options.scales.xAxes[0].time.unit = axisType;
    //     }
    //     if (changes && changes.data && changes.data.currentValue) {
    //         this.setChart();
    //     }
    // }

    getDateRande(lenght: number): Date[] {
        const startOfWeek = moment().subtract(lenght, 'd');
        const endOfWeek = moment().subtract(1, 'd');
        const days = [];
        let day = startOfWeek;

        while (day <= endOfWeek) {
            days.push(day.toDate());
            day = day.clone().add(1, 'd');
        }

        return days;
    }

    setChart(): void {
        const ctx = this.canvas.nativeElement.getContext('2d');
        if (this.chart) this.chart.destroy();
        this.chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: this.getDateRande(7),
                datasets: this.chartData,
            },
            options: this.options,
        });
    }
}
