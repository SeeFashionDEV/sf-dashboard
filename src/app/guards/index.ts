import { CanLoadDashboardGuard } from './can-load-dashboard.guard';

export const APP_GUARDS = [
    CanLoadDashboardGuard,
];