import { NgModule } from '@angular/core';
import { DASHBOARD_CORE_COMPONENTS } from './components';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../core/modules/material.module';
import {
    PerfectScrollbarModule, PerfectScrollbarConfigInterface,
    PERFECT_SCROLLBAR_CONFIG,
} from 'ngx-perfect-scrollbar';
import { DASHBOARD_MODALS } from './modals';
import { DashboarModalsModule } from './modals/dashboard-modals.module.ts.module';
import { DASHBOARD_PIPES } from './pipes';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true,
    wheelPropagation: true,
};

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        PerfectScrollbarModule,
        DashboarModalsModule,
    ],
    exports: [
        ...DASHBOARD_CORE_COMPONENTS,
        ...DASHBOARD_PIPES,
    ],
    declarations: [
        ...DASHBOARD_CORE_COMPONENTS,
        ...DASHBOARD_PIPES,
    ],
    entryComponents: [
    ],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
        },
    ],
})
export class DashboardCoreModule { }
