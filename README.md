# SeeFashion

## Development mode

Create a config.json file in /config directory. A template is provided in the same directory.
Run `npm install` to install dependencies.
Run `npm run start` for a dev server. Navigate to `http://localhost:8000/`. The app will automatically reload if you change any of the source files.

## Production mode

Create a config.json file in /config directory. A template is provided in the same directory.
Run `npm install` to install dependencies.
Run `npm run build:prod` to build the project. The build artifacts will be stored in the `dist/` directory.



test commit
