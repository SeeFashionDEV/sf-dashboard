export enum DataType {
    NUMERIC,
    CURRENCY,
    PERCENTAGE,
}