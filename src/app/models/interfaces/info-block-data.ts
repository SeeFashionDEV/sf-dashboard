import { DataType } from '../enums/data-type';
import { IData } from './data';

interface InfoBlockBar1Data {
    standard: IData;
    seeFashion: IData;
}

interface InfoBlockBar2Data {
    standard: number;
    seeFashion: number;
}

export interface IInfoBlockData {
    title?: string;
    value: number;
    dataType?: DataType;
    kpi?: number;
    bar1?: InfoBlockBar1Data;
    bar2?: InfoBlockBar2Data;
}