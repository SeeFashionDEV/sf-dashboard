import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../../core/modules/material.module';
import { DASHBOARD_MODALS } from '.';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
    ],
    exports: [
        ...DASHBOARD_MODALS,
    ],
    declarations: [
        ...DASHBOARD_MODALS,
    ],
    entryComponents: [
        ...DASHBOARD_MODALS,
    ],
})
export class DashboarModalsModule { }
