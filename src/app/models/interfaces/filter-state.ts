import { FilterRange } from '../enums/filter-range';

export interface IFilterState {
    range?: FilterRange;
    custom?: {
        startDate: string;
        endDate: string;
    };
}
