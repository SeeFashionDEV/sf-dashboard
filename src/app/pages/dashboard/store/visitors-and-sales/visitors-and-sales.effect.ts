// import { Injectable } from '@angular/core';
// import { Effect, Actions, ofType } from '@ngrx/effects';
// import { Observable, of, empty } from 'rxjs';
// import {
//     VisitorsAndSalesActions, LoadVisitorsAndSalesDataAction, VisitorsAndSalesTypes,
// } from './visitors-and-sales.action';
// import { AppState } from '../../../../store/index';
// import { Store } from '@ngrx/store';
// import { withLatestFrom, switchMap } from 'rxjs/operators';
// import { getFilterState } from '../index';
// import { FilterRange } from '../../../../models/enums/filter-range';
// import { DataType } from '../../../../models/enums/data-type';
// import { FilterActions, FilterTypes } from '../filter/filter.action';
// import { IVisitorsData, IAddToCartData, ISalesData } from '../../../../models/interfaces/visitors-and-sales';
// import { IInfoBlockData } from '../../../../models/interfaces/info-block-data';
//
// @Injectable()
// export class VisitorsAndSalesEffects {
//
//     @Effect()
//     loadData$: Observable<VisitorsAndSalesActions> = this.actions$.pipe(
//         ofType<LoadVisitorsAndSalesDataAction>(VisitorsAndSalesTypes.LoadData),
//         withLatestFrom(this.store.select(getFilterState)),
//         switchMap(([action, filter]) => {
//             if (!filter) return empty();
//             const visitors: IVisitorsData = this.getVisitors(filter.range);
//             const addToCard: IAddToCartData = this.getAddToCard(filter.range);
//             const sales: ISalesData = this.getSales(filter.range);
//
//             return of(new LoadVisitorsAndSalesDataSuccessAction({
//                 visitors: visitors,
//                 addToCard: addToCard,
//                 sales: sales,
//             }));
//         }),
//     );
//
//     @Effect()
//     reloadData$: Observable<VisitorsAndSalesActions> = this.actions$.pipe(
//         ofType<FilterActions>(FilterTypes.ChangeFilterState),
//         switchMap(() => of(new LoadVisitorsAndSalesDataAction())),
//     );
//
//     constructor(
//         private actions$: Actions,
//         private store: Store<AppState>,
//     ) { }
//
//     getVisitors(range: FilterRange): IVisitorsData {
//         if (range == null) return;
//         let uniqueVisitors: IInfoBlockData;
//         let numberOfProductsViewed: IInfoBlockData;
//         let averageTimePerProductView: IInfoBlockData;
//         let numberOfRecomendations: IInfoBlockData;
//         const v = {
//             uniqueVisitors: {
//                 title: 'unique visitors',
//                 dataType: DataType.NUMERIC,
//             },
//             numberOfProductsViewed: {
//                 title: 'number of products viewed',
//                 dataType: DataType.NUMERIC,
//             },
//             averageTimePerProductView: {
//                 title: 'average time spent per product view',
//                 dataType: DataType.NUMERIC,
//             },
//             numberOfRecomendations: {
//                 title: 'number of recomendataions server per visitor',
//                 dataType: DataType.NUMERIC,
//             },
//         };
//         switch (range) {
//             case FilterRange.MONTH:
//                 uniqueVisitors = {
//                     value: 5321,
//                     kpi: 5,
//                 };
//                 numberOfProductsViewed = {
//                     value: 9,
//                     kpi: 0,
//                     bar1: {
//                         standard: {
//                             value: 7,
//                             kpi: 1,
//                         },
//                         seeFashion: {
//                             value: 11,
//                             kpi: -2,
//                         },
//                     },
//                 };
//                 averageTimePerProductView = {
//                     value: 21,
//                     kpi: 2,
//                     bar1: {
//                         standard: {
//                             value: 18,
//                             kpi: -1,
//                         },
//                         seeFashion: {
//                             value: 24,
//                             kpi: 5,
//                         },
//                     },
//                 };
//                 numberOfRecomendations = {
//                     value: 61,
//                     kpi: 1,
//                 };
//                 break;
//             case FilterRange.WEEK:
//                 uniqueVisitors = {
//                     value: 1170,
//                     kpi: 5,
//                 };
//                 numberOfProductsViewed = {
//                     value: 10,
//                     kpi: 0,
//                     bar1: {
//                         standard: {
//                             value: 8,
//                             kpi: 5,
//                         },
//                         seeFashion: {
//                             value: 12,
//                             kpi: -5,
//                         },
//                     },
//                 };
//                 averageTimePerProductView = {
//                     value: 19,
//                     kpi: 4,
//                     bar1: {
//                         standard: {
//                             value: 14,
//                             kpi: -4,
//                         },
//                         seeFashion: {
//                             value: 24,
//                             kpi: 12,
//                         },
//                     },
//                 };
//                 numberOfRecomendations = {
//                     value: 54,
//                     kpi: 1,
//                 };
//                 break;
//             case FilterRange.TODAY:
//                 uniqueVisitors = {
//                     value: 129,
//                     kpi: 0,
//                 };
//                 numberOfProductsViewed = {
//                     value: 9,
//                     kpi: 0,
//                     bar1: {
//                         standard: {
//                             value: 7,
//                             kpi: 2.5,
//                         },
//                         seeFashion: {
//                             value: 11,
//                             kpi: -2.5,
//                         },
//                     },
//                 };
//                 averageTimePerProductView = {
//                     value: 15,
//                     kpi: 2,
//                     bar1: {
//                         standard: {
//                             value: 22,
//                             kpi: -2,
//                         },
//                         seeFashion: {
//                             value: 8,
//                             kpi: 6,
//                         },
//                     },
//                 };
//                 numberOfRecomendations = {
//                     value: 64,
//                     kpi: 0.5,
//                 };
//                 break;
//         }
//         return {
//             uniqueVisitors: {
//                 ...v.uniqueVisitors,
//                 ...uniqueVisitors,
//             },
//             numberOfProductsViewed: {
//                 ...v.numberOfProductsViewed,
//                 ...numberOfProductsViewed,
//             },
//             averageTimePerProductView: {
//                 ...v.averageTimePerProductView,
//                 ...averageTimePerProductView,
//             },
//             numberOfRecomendations: {
//                 ...v.numberOfRecomendations,
//                 ...numberOfRecomendations,
//             },
//         };
//     }
//
//     getAddToCard(range: FilterRange): IAddToCartData {
//         if (range == null) return;
//         let total: IInfoBlockData;
//         let sfDriven: IInfoBlockData;
//         let averageNumberOfItemsPerVisitor: IInfoBlockData;
//         const v = {
//             total: {
//                 title: 'total add to carts',
//                 dataType: DataType.NUMERIC,
//             },
//             sfDriven: {
//                 title: 'total value of add to carts',
//                 dataType: DataType.NUMERIC,
//             },
//             averageNumberOfItemsPerVisitor: {
//                 title: 'average cart value per visitor',
//                 dataType: DataType.NUMERIC,
//             },
//         };
//         switch (range) {
//             case FilterRange.MONTH:
//                 total = {
//                     value: 4799,
//                     kpi: 5,
//                 };
//                 sfDriven = {
//                     value: 145019.5,
//                     kpi: 4,
//                     bar1: {
//                         seeFashion: {
//                             value: 43505.8,
//                             kpi: 3,
//                         },
//                         standard: {
//                             value: 101513.6,
//                             kpi: 1,
//                         },
//                     },
//                 };
//                 averageNumberOfItemsPerVisitor = {
//                     value: 0.85,
//                     kpi: 3,
//                     bar1: {
//                         seeFashion: {
//                             value: 0.9,
//                             kpi: 5,
//                         },
//                         standard: {
//                             value: 0.8,
//                             kpi: 2,
//                         },
//                     },
//                 };
//                 break;
//             case FilterRange.WEEK:
//                 total = {
//                     value: 996,
//                     kpi: 1,
//                 };
//                 sfDriven = {
//                     value: 31187,
//                     kpi: 7,
//                     bar1: {
//                         seeFashion: {
//                             value: 11227.3,
//                             kpi: 10,
//                         },
//                         standard: {
//                             value: 19959.6,
//                             kpi: 4,
//                         },
//                     },
//                 };
//                 averageNumberOfItemsPerVisitor = {
//                     value: 0.8,
//                     kpi: 9,
//                     bar1: {
//                         seeFashion: {
//                             value: 0.9,
//                             kpi: 12,
//                         },
//                         standard: {
//                             value: 0.6,
//                             kpi: 6,
//                         },
//                     },
//                 };
//                 break;
//             case FilterRange.TODAY:
//                 total = {
//                     value: 145,
//                     kpi: 3,
//                 };
//                 sfDriven = {
//                     value: 4798,
//                     kpi: 3.5,
//                     bar1: {
//                         seeFashion: {
//                             value: 1055.5,
//                             kpi: 5,
//                         },
//                         standard: {
//                             value: 3742.4,
//                             kpi: 2,
//                         },
//                     },
//                 };
//                 averageNumberOfItemsPerVisitor = {
//                     value: 1.1,
//                     kpi: 4.5,
//                     bar1: {
//                         seeFashion: {
//                             value: 0.9,
//                             kpi: 6,
//                         },
//                         standard: {
//                             value: 1.2,
//                             kpi: 3,
//                         },
//                     },
//                 };
//                 break;
//         }
//         return {
//             totalAddToCarts: {
//                 ...v.total,
//                 ...total,
//             },
//             totalValueOfAddToCarts: {
//                 ...v.sfDriven,
//                 ...sfDriven,
//             },
//             averageNumberOfItemsPerVisitor: {
//                 ...v.averageNumberOfItemsPerVisitor,
//                 ...averageNumberOfItemsPerVisitor,
//             },
//         };
//     }
//
//     getSales(range: FilterRange): ISalesData {
//         if (range == null) return;
//         let revenues: IInfoBlockData;
//         let revenuePerVisit: IInfoBlockData;
//         let revenuePerProductView: IInfoBlockData;
//         let averageOrderSize: IInfoBlockData;
//         const v = {
//             revenues: {
//                 title: 'revenues',
//                 dataType: DataType.NUMERIC,
//             },
//             revenuePerVisit: {
//                 title: 'revenue per visit',
//                 dataType: DataType.NUMERIC,
//             },
//             revenuePerProductView: {
//                 title: 'revenue per product view',
//                 dataType: DataType.NUMERIC,
//             },
//             averageOrderSize: {
//                 title: 'average order size',
//                 dataType: DataType.NUMERIC,
//             },
//         };
//         switch (range) {
//             case FilterRange.MONTH:
//                 revenues = {
//                     value: 145019.5,
//                     kpi: 4,
//                 };
//                 revenuePerVisit = {
//                     value: 27.2,
//                 };
//                 revenuePerProductView = {
//                     value: 3,
//                     kpi: 1,
//                 };
//                 averageOrderSize = {
//                     value: 139,
//                     kpi: 3,
//                     bar1: {
//                         seeFashion: {
//                             value: 157,
//                             kpi: 4,
//                         },
//                         standard: {
//                             value: 121,
//                             kpi: 1,
//                         },
//                     },
//                 };
//                 break;
//             case FilterRange.WEEK:
//                 revenues = {
//                     value: 31187,
//                     kpi: 7.5,
//                 };
//                 revenuePerVisit = {
//                     value: 26.6,
//                 };
//                 revenuePerProductView = {
//                     value: 2.6,
//                     kpi: 3.5,
//                 };
//                 averageOrderSize = {
//                     value: 144,
//                     kpi: 7,
//                     bar1: {
//                         seeFashion: {
//                             value: 168,
//                             kpi: 10,
//                         },
//                         standard: {
//                             value: 120,
//                             kpi: 4,
//                         },
//                     },
//                 };
//                 break;
//             case FilterRange.TODAY:
//                 revenues = {
//                     value: 4798,
//                     kpi: 3.7,
//                 };
//                 revenuePerVisit = {
//                     value: 37.2,
//                 };
//                 revenuePerProductView = {
//                     value: 4.1,
//                     kpi: 1.7,
//                 };
//                 averageOrderSize = {
//                     value: 152.5,
//                     kpi: 3.5,
//                     bar1: {
//                         seeFashion: {
//                             value: 165,
//                             kpi: 5,
//                         },
//                         standard: {
//                             value: 140,
//                             kpi: 2,
//                         },
//                     },
//                 };
//                 break;
//         }
//         return {
//             revenues: {
//                 ...v.revenues,
//                 ...revenues,
//             },
//             revenuePerVisit: {
//                 ...v.revenuePerVisit,
//                 ...revenuePerVisit,
//             },
//             revenuePerProductView: {
//                 ...v.revenuePerProductView,
//                 ...revenuePerProductView,
//             },
//             averageOrderSize: {
//                 ...v.averageOrderSize,
//                 ...averageOrderSize,
//             },
//         };
//     }
// }
